﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreData;

public class TypeMatch{ 

    int[,] bolwerInc = new int[,]
    {
        {-1,-1,-1,0,0,0},
        {-1,-1,-1,0,0,0},
        {-1,-1,-1,0,0,0},
        {1,0,1,1,1,1},
        {0,1,1,1,1,1},
        {1,1,0,1,1,1}
    };

    int[,] batsmanInc = new int[,]
    {
        {1,1,1,0,0,0},
        {1,1,1,0,0,0},
        {1,1,1,0,0,0},
        {1,1,0,0,0,0},
        {1,0,1,0,0,0},
        {0,1,1,0,0,0}
    };

    public int getBowlingAttackIncrease(PlayerType batType, PlayerType bowlType)
    {
        int index1 = (int)batType;
        int index2 = (int)bowlType;

        //Debug.Log("returning incresed bowling" + bolwerInc[index1, index2]);
        return bolwerInc[index1,index2]; 
    }

    public int getBattingAttackIncrease(PlayerType batType, PlayerType bowlType)
    {
        int index1 = (int)batType;
        int index2 = (int)bowlType;
        //Debug.Log("returning incresed batting" + batsmanInc[index1, index2]);
        return batsmanInc[index1,index2]; 
    }
	
}
