﻿using UnityEngine;
using System.Collections;

public class RunScoringTable {

    //batsman atk vs Run
    int[,] runScoreTable = new int[,]
    {
        {50,20,10,10,9,1},
        {45,25,10,10,8,2},
        {40,25,15,10,7,3},
        {35,25,15,15,6,4},
        {30,25,20,15,5,5},
        {25,20,25,15,9,6},
        {20,15,25,20,13,7},
        {15,10,25,25,17,8},
        {10,5,25,25,26,9},
        {5,5,25,25,30,10},
    };

    int[,] scoreTable = new int[10, 6]; 

    public RunScoringTable()
    {
        SetRunProbability();
        //PrintProbability();
    }

    public void SetRunProbability()
    {
        for (int i = 0; i < 10; i++)
        {
            int jSum = 0;
            for (int j = 0; j < 6; j++)
            {
                if (j == 0)
                    scoreTable[i, j] = runScoreTable[i, j];
                else
                {
                    scoreTable[i, j] = jSum + runScoreTable[i, j];   
                }

                jSum = scoreTable[i, j];

            }
        }
    }

    void PrintProbability()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                Debug.Log(i + "," + j + " value : " + scoreTable[i,j]);
            }
        }
    }

    public int getRunProbability(int batsmanATK, int receivedRandom)
    {
        batsmanATK = batsmanATK - 1;

        if(receivedRandom <= scoreTable[batsmanATK,0])
            return 0;

        else if(receivedRandom <= scoreTable[batsmanATK,1])
            return 1;

        else if(receivedRandom <= scoreTable[batsmanATK,2])
            return 2;

        else if(receivedRandom <= scoreTable[batsmanATK,3])
            return 3;

        else if(receivedRandom <= scoreTable[batsmanATK,4])
            return 4;

        else
            return 6;

    }
	
}
