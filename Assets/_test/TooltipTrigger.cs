﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TooltipTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler,IPointerClickHandler,IScrollHandler{

        public string text;

        public void OnPointerEnter(PointerEventData eventData)
        {
            StartHover(new Vector3(eventData.position.x, eventData.position.y - 18f, 0f));
        }
        public void OnSelect(BaseEventData eventData)
        {
            StartHover(transform.position);
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            StopHover();
        }
        public void OnDeselect(BaseEventData eventData)
        {
            StopHover();
        }

        void StartHover(Vector3 position)
        {
            //TooltipView.Instance.ShowTooltip(text, position);
            Debug.Log("its called");
        }
        void StopHover()
        {
            //TooltipView.Instance.HideTooltip();
            Debug.Log("its called out");
        }


        public void OnPointerClick(PointerEventData eventData){
            Debug.Log("clicked");
        }

        public void OnScroll(PointerEventData eventData){
            Debug.Log("Scrool started");
        }
}
