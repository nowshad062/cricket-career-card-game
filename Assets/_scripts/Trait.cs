﻿namespace CoreData{
	public class Trait{
        protected string name;
        protected string description;

        public Trait()
        {

        }

        public Trait(string name, string description)
        {
            this.name = name;
            this.description = description;
        }

        public string Name
        {
            get { return name; }
        }
        public string Description
        {
            get { return description; }
        }

        public void Reset()
        {

        }

        public abstract void Apply();
        public abstract void Release();

        public abstract void UpdateData(PlayerCard playerCard);
       
	}
}
