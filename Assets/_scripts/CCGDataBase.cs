﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreData;

[CreateAssetMenu(fileName = "CCGData", menuName = "CCG/Data", order = 1)]
public class CCGDataBase : ScriptableObject {
	public List<PlayerInfo> playerInfos;
    public List<TraitInfo> traitInfos;
}
