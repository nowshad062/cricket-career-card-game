﻿namespace CoreData{

	public enum PlayerType{
		AGRESSIVE,
		TECHNICAL,
		DEFENSIVE,
		PACER,
		MEDIUM_PACER,
		SPINNER
	}

	public enum TraitType{
		DYING_WISH,
		UNBREAKABLE_WILL,
		INDOMITABLE_FORCE,
		RAISE_MORALE,
		ENRAGE,
		UP_THE_SLEEVE,
		TAG,
		COUNTER,
		FREEZE,
		IMMUNE,
		MASTER_BLASTER,
		SILENCE,
		BATTLECRY,
		INTIMIDATE,
		TRANSFER,
		BRIBE,
		BRIBE_2,
        NONE
	}

    public enum TraitDurationType
    {
        ONE_TIME,
        LIMITED_TIME,
        LIFE_TIME
    }

	[System.Serializable]
	public struct Ability{
		public int healthPoint;
		public int attackPoint;
	}
		
	[System.Serializable]
	public struct PlayerInfo{
		public string name;
		public PlayerType playerType;
		public TraitType trait;
		public Ability commonAbility;
		public Ability rareAbility;
		public Ability legandaryAbility;
	}

    [System.Serializable]
    public struct TraitInfo
    {
       public TraitType name;
       public int duration;
       public int commonValue;
       public int rareValue;
       public int legendaryValue;
       
    }
  

	public class PreSetupStructure {
	}
}
