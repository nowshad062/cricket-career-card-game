﻿using System.Collections;

namespace CoreData{
	public class Cricketer{
      
		protected string name;
		protected int healthPoint;
		protected int attackPoint;

		public string Name {
			get {
				return name;
			}
		}

		public int HealthPoint {
			get {
				return healthPoint;
			}
		}

		public int AttackPoint {
			get {
				return attackPoint;
			}
		}
	}

	public class Batsman : Cricketer{
		
	}

	public class Bowler : Cricketer{
		
	}

	public class Aggressive : Batsman{
		public Aggressive(string name, int attackPoint, int healthPoint){
			this.name = name;
			this.attackPoint = attackPoint;
			this.healthPoint = healthPoint;
		}
	}

	public class Defensive : Batsman{
		public Defensive(string name, int attackPoint, int healthPoint){
			this.name = name;
			this.attackPoint = attackPoint;
			this.healthPoint = healthPoint;
		}
	}

	public class Technical : Batsman{
		public Technical(string name, int attackPoint, int healthPoint){
			this.name = name;
			this.attackPoint = attackPoint;
			this.healthPoint = healthPoint;
		}
	}

	public class Pacer : Bowler {
		public Pacer(string name, int attackPoint, int healthPoint){
			this.name = name;
			this.attackPoint = attackPoint;
			this.healthPoint = healthPoint;
		}
	}

	public class Spiner : Bowler{
		public Spiner(string name, int attackPoint, int healthPoint){
			this.name = name;
			this.attackPoint = attackPoint;
			this.healthPoint = healthPoint;
		}
	}

	public class MediumPacer : Bowler{
		public MediumPacer(string name, int attackPoint, int healthPoint){
			this.name = name;
			this.attackPoint = attackPoint;
			this.healthPoint = healthPoint;
		}
	}
}
