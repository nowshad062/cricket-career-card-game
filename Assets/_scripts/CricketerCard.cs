﻿using UnityEngine;
using System.Collections;

namespace CoreData{
	public class CricketerCard{
		int id;
		Cricketer cricketer;
		Trait trait;

		public CricketerCard(int id, Cricketer cricketer, Trait trait){
			this.id = id;
			this.cricketer = cricketer;
			this.trait = trait;
		}
			
		public int Id {
			get {
				return id;
			}
		}

		public Cricketer Cricketer {
			get {
				return cricketer;
			}
		}

		public Trait Trait {
			get {
				return trait;
			}
		}
	}
}