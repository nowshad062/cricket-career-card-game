﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using CoreData;

public class MatchSimUI : MonoBehaviour
{

    //hud
    public Text team1;
    public Text team2;

    //Card Slot
    public GameObject player1Slot;
    public GameObject player2Slot;

    //Teams
    Team Player1Team;
    Team Player2Team;

    PlayerCard Player1Card;
    PlayerCard Player2Card;
    public PlayerCardInfo player1CardInfo;
    public PlayerCardInfo player2CardInfo;


    //indexes
    int Player1CardIndex;
    int Player2CardIndex;

    //score
    int ballCounter = 0;
    int totalScore = 0;
    int over = 0;
    int runInThisBall = 0;
    List<int> runThisOver;


    //Managers
    PlayerManager playerManager;
    TeamManager teamManager;

    //Button
    public Button playButton;

    public List<Button> player1Cards;
    public List<Button> player2Cards;

    //ScorePanel
    public GameObject runInThisOverText;
    public GameObject targetImage;
    public Text overText;
    public Text scoreText;
    public Text targetText;

    //simulation
    Simulation simulation;
    RunScoringTable runScoringTable;


    private System.Random rng;
    int lastball = 6;

    int target = -1;
    int numOfWicketsFall;
    int numOfBowlersFall;

    bool gamePlayerBatting;

    void Awake()
    {
        playerManager = Factory.getPlayerManager();
        teamManager = Factory.getTeamrManager();
        player1Cards = new List<Button>();
        player2Cards = new List<Button>();
        runScoringTable = new RunScoringTable();
        rng = new System.Random();
        runThisOver = new List<int>();
        numOfWicketsFall = 0;
        numOfBowlersFall = 0;
        gamePlayerBatting = false;

        playButton.onClick.AddListener(delegate
        {
            //playButton.interactable = false;
            StartCoroutine(RunSimulation());
            //StartCoroutine(PlayOverRoutine());
            //StartCoroutine(PlayRestOfTheGame());
        });
    }

    IEnumerator PlayOverRoutine()
    {

        if (gamePlayerBatting)
        {

            int i;

            for (i = 0; i < lastball; i++)
            {
                if (Player1Card == null)
                    break;
                yield return StartCoroutine(RunSimulation());

            }

            if (Player1Card == null)
                lastball = 6 - i;
            else
                lastball = 6;

            if (Player1Card != null)
                playButton.interactable = true;
        }
        else
        {

            int i;

            for (i = 0; i < lastball; i++)
            {
                if (Player2Card == null)
                    break;
                yield return StartCoroutine(RunSimulation());

            }

            if (Player2Card == null)
                lastball = 6 - i;
            else
                lastball = 6;

            if (Player2Card != null)
                playButton.interactable = true;
        }
    }

    IEnumerator RunSimulation()
    {
        yield return new WaitForSeconds(0.1f);

        if (Player1Card != null && Player2Card != null)
        {
            TypeMatchUpCalculation();

            GetComponent<Simulation>().Simulate(Player1Card, Player2Card);

            if (gamePlayerBatting)
                RunCalculation(Player1Card);
            else
                RunCalculation(Player2Card);


            BallCalculation();

            Player1Card.RestoreDefault();
            Player2Card.RestoreDefault();

            player1CardInfo.SetPlayerInfo(Player1Card);
            player2CardInfo.SetPlayerInfo(Player2Card);


            ListenDeadEvent();


        }
    }

    IEnumerator PlayRestOfTheGame()
    {
        PlayerCard dummy = new PlayerCard();
        dummy.AttackPoint = 0;
        dummy.HitPoint = 0;
        dummy.TraitCard = new NONE();

        Player2Card = dummy;



        while (this.ballCounter < 120)
        {
            RunCalculation(Player1Card);
            BallCalculation();

            Player1Card.RestoreDefault();
            player1CardInfo.SetPlayerInfo(Player1Card);

            yield return new WaitForSeconds(0.1f);
        }

        EndOfInnings();

    }


    void EndOfInnings()
    {

        if (target == -1)
        {
            target = totalScore + 1;
            targetImage.SetActive(true);
            targetText.text = target.ToString();

            Debug.Log("Target: " + target);
        }

        StartCoroutine(StartSecondInnings());
    }

    IEnumerator StartSecondInnings()
    {
        Debug.Log("Starting 2nd Innings");
        yield return new WaitForSeconds(3f);

        StartOfInnings();

    }

    void StartOfInnings()
    {
        gamePlayerBatting = !gamePlayerBatting;
        totalScore = 0;
        this.ballCounter = 0;
        lastball = 6;
        Player1Card = null;
        player1CardInfo.gameObject.SetActive(false);
        Player2Card = null;//???start with batsman
        player2CardInfo.gameObject.SetActive(false);

        numOfWicketsFall = 0;
        numOfBowlersFall = 0;

        //revive players
        for (int i = 0; i < Player1Team.AllPlayers.Count; i++)
        {
            PlayerCard player = playerManager.getPlayer(Player1Team.AllPlayers[i]);
            player.HitPoint = 30;
        }

        for (int i = 0; i < Player2Team.AllPlayers.Count; i++)
        {
            PlayerCard player = playerManager.getPlayer(Player2Team.AllPlayers[i]);
            player.HitPoint = 30;
        }

        playButton.interactable = false;

    }

    public void Initialize()
    {
        Player1Team = Factory.getTeamrManager().getTeam(0);
        Player2Team = Factory.getTeamrManager().getTeam(2);

        team1.text = Player1Team.Name;
        team2.text = Player2Team.Name;

        FillUpPlayer1Team(Player1Team);
        FillUpPlayer2Team(Player2Team);

        playButton.interactable = false;

        DecideTurn();
    }

    void DecideTurn()
    {
        if (gamePlayerBatting)
        {

        }
        else
        {
            SelectRandomPlayer2Card();
        }
    }

    void FillUpPlayer2Team(Team Player2Team)
    {
        for (int i = 0; i < Player2Team.AllPlayers.Count; i++)
        {
            PlayerCard player = playerManager.getPlayer(Player2Team.AllPlayers[i]);
            player2Cards.Add(player2Slot.transform.GetChild(i).GetComponent<Button>());
            AddPlayer2CardListener(player2Cards[i], i);

        }
    }

    void FillUpPlayer1Team(Team Player1Team)
    {
        for (int i = 0; i < Player1Team.AllPlayers.Count; i++)
        {
            PlayerCard player = playerManager.getPlayer(Player1Team.AllPlayers[i]);
            player1Cards.Add(player1Slot.transform.GetChild(i).GetComponent<Button>());
            AddPlayer1CardListener(player1Cards[i], i);

        }

    }

    void AddPlayer1CardListener(Button b, int value)
    {
        b.onClick.AddListener(() => ChosePlayer1Card(value));
    }

    void AddPlayer2CardListener(Button b, int value)
    {
        b.onClick.AddListener(() => ChosePlayer2Card(value));
    }

    void ChosePlayer1Card(int index)
    {
        PlayerCard chosenPlayer = playerManager.getPlayer(Player1Team.AllPlayers[index]);

        if (Player1Card == null)
        {
            player1CardInfo.gameObject.SetActive(true);
            player1Cards[index].gameObject.SetActive(false);

            if (gamePlayerBatting && chosenPlayer.isBowler())
                HalfHP(chosenPlayer);

            if (!gamePlayerBatting && !chosenPlayer.isBowler())
                HalfHP(chosenPlayer);

            Player1Card = chosenPlayer;
            player1CardInfo.SetPlayerInfo(Player1Card);
            Player1CardIndex = index;

            if (gamePlayerBatting)
            {
                SelectRandomPlayer2Card();
               
            }

            playButton.interactable = true;

        }
    }

    void ChosePlayer2Card(int index)
    {
        PlayerCard AIPlayer = playerManager.getPlayer(Player2Team.AllPlayers[index]);

        if (Player2Card == null)
        {

            player2CardInfo.gameObject.SetActive(true);
            player2Cards[index].gameObject.SetActive(false);

            if (gamePlayerBatting && !AIPlayer.isBowler())
                HalfHP(AIPlayer);

            if (!gamePlayerBatting && AIPlayer.isBowler())
                HalfHP(AIPlayer);


            Player2Card = AIPlayer;
            player2CardInfo.SetPlayerInfo(Player2Card);
            Player2CardIndex = index;
        }

    }

    void SelectRandomPlayer2Card()
    {
        List<int> randomPlayerIndexes = new List<int>();

        for (int i = 0; i < Player2Team.AllPlayers.Count; i++)
        {
            PlayerCard randomPlayer = playerManager.getPlayer(Player2Team.AllPlayers[i]);

            if (randomPlayer.HitPoint > 0)
            {
                randomPlayerIndexes.Add(i);
            }
        }

        if (randomPlayerIndexes.Count > 0)
        {
            Shuffle(randomPlayerIndexes);
            int bIndex = randomPlayerIndexes[0];
            player2Cards[bIndex].GetComponent<Button>().onClick.Invoke();
        }

    }


    void TypeMatchUpCalculation()
    {

        PlayerType batsmanType;
        PlayerType bowlerType;
        TypeMatch match = new TypeMatch();

        if (gamePlayerBatting)
        {
            batsmanType = Player1Card.PlayerType;
            bowlerType = Player2Card.PlayerType;
            Player1Card.AttackPoint += match.getBattingAttackIncrease(batsmanType, bowlerType);
            Player2Card.AttackPoint += match.getBowlingAttackIncrease(batsmanType, bowlerType);
        }
        else
        {
            batsmanType = Player2Card.PlayerType;
            bowlerType = Player1Card.PlayerType;
            Player1Card.AttackPoint += match.getBowlingAttackIncrease(batsmanType, bowlerType);
            Player2Card.AttackPoint += match.getBattingAttackIncrease(batsmanType, bowlerType);
        }

    }

    void BallCalculation()
    {
        this.ballCounter++;

        if ((ballCounter % 6) == 0)
        {
            over++;
            StartCoroutine(OverDone());
        }

        UpdateScoreBoard();
    }

    IEnumerator OverDone()
    {
        //Over done
        Debug.Log("Over Done! Switch Bowler");

        yield return new WaitForSeconds(0.5f);

       

        if (gamePlayerBatting)
        {
            RetirePlayer2Card();
            SelectRandomPlayer2Card();
        }
        else
        {
            RetirePlaye1Card();
        }

    
        runThisOver.Clear();

    }

    void RetirePlaye1Card()
    {
        if (Player1Card!= null && Player1Card.HitPoint > 0)
        {
            playButton.interactable = false;
            player1CardInfo.gameObject.SetActive(false);
            player1Cards[Player1CardIndex].gameObject.SetActive(true);
            Player1Card = null;
        }
    }

    void RetirePlayer2Card()
    {
        if (Player2Card != null && Player2Card.HitPoint > 0)
        {
            playButton.interactable = false;
            player2CardInfo.gameObject.SetActive(false);
            player2Cards[Player2CardIndex].gameObject.SetActive(true);
            //player2CardInfo.SetPlayerInfo(Player2Card);
            Player2Card = null;
        }
    }

    void RunCalculation(PlayerCard batsman)
    {
        int random = Random.Range(0, 100);
        runInThisBall = runScoringTable.getRunProbability(batsman.AttackPoint, random);
        totalScore = (totalScore + runInThisBall);
        runThisOver.Add(runInThisBall);
        UpdateScoreBoard();

    }

    void UpdateScoreBoard()
    {
        string run = "";

        for (int i = 0; i < 6; i++)
        {

            if (i < runThisOver.Count)
            {
                run = runThisOver[i].ToString();

            }
            else
                run = "";

            runInThisOverText.transform.GetChild(i).GetComponent<Text>().text = run;
        }

        overText.text = over.ToString() + "." + (this.ballCounter % 6).ToString();
        scoreText.text = totalScore.ToString();
    }

    void HalfHP(PlayerCard player)
    {
        player.HitPoint /= 2;
    }

    void ListenDeadEvent()
    {
        if (Player1Card.HitPoint <= 0)
        {
            Debug.Log("Player1 Card Dead");
            Player1Card.TraitCard.IsDead = true;
            Player1Card = null;
            player1CardInfo.gameObject.SetActive(false);
            numOfWicketsFall++;

            if (gamePlayerBatting && numOfWicketsFall > 10)
                BattingTeamAllOut();
            if (!gamePlayerBatting && numOfBowlersFall > 10)
                BowlingTeamAllout();
        }

        if (Player2Card.HitPoint <= 0)
        {
            Debug.Log("Player2 Card Dead");
            Player2Card.TraitCard.IsDead = true;
            Player2Card = null;
            player2CardInfo.gameObject.SetActive(false);
            numOfBowlersFall++;

            if (numOfBowlersFall > 10)
                BowlingTeamAllout();
            else
                SelectRandomPlayer2Card();
        }
    }

    void BattingTeamAllOut()
    {
        EndOfInnings();
    }

    void BowlingTeamAllout()
    {
        playButton.gameObject.SetActive(false);
        StartCoroutine(PlayRestOfTheGame());
    }

    public void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
