﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerCardInfo : MonoBehaviour {
    Image flagImage;
    Image playerImage;
    Image cardBaseImage;
    Text hpValue;
    Text energyValue;
    Text attackValue;
    Text playerName;
    Text playerType;
    Text traitDetails;

    void Awake(){
        InitializeUi();
    }

    private void InitializeUi(){
        flagImage = transform.GetChild(0).GetComponent<Image>();
        playerImage = transform.GetChild(1).GetComponent<Image>();
        cardBaseImage = transform.GetChild(2).GetComponent<Image>();
        hpValue = cardBaseImage.transform.GetChild(0).GetComponent<Text>();
        energyValue = cardBaseImage.transform.GetChild(1).GetComponent<Text>();
        attackValue = cardBaseImage.transform.GetChild(2).GetComponent<Text>();
        playerName = cardBaseImage.transform.GetChild(3).GetComponent<Text>();
        playerType = cardBaseImage.transform.GetChild(4).GetComponent<Text>();
        traitDetails = cardBaseImage.transform.GetChild(5).GetComponent<Text>();
    }

    public void SetPlayerInfo(PlayerCard player){
        //no flag image for now
        //cardBaseImage.sprite = SpriteManager.spriteManager.GetPlayerCategorySprite(player.PlayerCategory.ToString());
        hpValue.text = player.HitPoint.ToString();
        energyValue.text = player.EnergyCost.ToString();
        attackValue.text = player.AttackPoint.ToString();
        playerName.text = player.PlayerName;
        playerType.text = player.PlayerType.ToString();
        traitDetails.text = player.TraitCard.Description;
    }
}
