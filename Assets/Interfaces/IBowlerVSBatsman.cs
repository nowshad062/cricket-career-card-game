﻿using CoreData;

public interface IBowlerVSBatsman
{
	void ApplyOneTimeTrait (Trait trait);

	void ApplyTraitWithDuration (Trait trait);
}
