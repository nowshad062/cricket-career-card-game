﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Simulation : MonoBehaviour {

    int ballPassed = 0;

    public int BallPassed
    {
        get { return ballPassed; }
        set { ballPassed = value; }
    }
    private int runScored = 0;


    public int RunScored
    {
        get { return runScored; }
        set { runScored = value; }
    }

    public void Simulate(PlayerCard batsman, PlayerCard bowler)
    {

        if (batsman.TraitCard.IsDead)
        {
            Debug.Log("Card Died");
            DoCardDamage(batsman, bowler);
        }
        else
        {

            if (batsman.TraitCard.ActiveDuration > 0)
            {
                DoCardDamage(batsman,bowler);
                batsman.TraitCard.ActiveDuration -= 1;
            }

            if (batsman.TraitCard.Ability == TraitAbility.DISABLE_TRAIT && batsman.TraitCard.ActiveDuration >= 0 && batsman.TraitCard.Ability != TraitAbility.AFFECT_FORCE_DAMAGE)

            {
                if (batsman.TraitCard.ActiveDuration == 0){
                    batsman.TraitCard.ActiveDuration = -1;
                }

            }

            else{

                Debug.Log("Doing Normal Damage");
                DoNormalDamage(batsman, bowler);

            }

        }
    }

    public void SimulateOnce(PlayerCard batsman, PlayerCard bowler)
    {
        //DoCardDamage(batsman, bowler);
    }

    void DoNormalDamage(PlayerCard batsman, PlayerCard bowler)
    {
        batsman.HitPoint = batsman.HitPoint - bowler.AttackPoint;
        bowler.HitPoint = bowler.HitPoint - batsman.AttackPoint;

        if (batsman.HitPoint <= 0)
        {
            batsman.TraitCard.IsDead = true;
            DoCardDamage(batsman, bowler);

        }

        if (bowler.HitPoint <= 0)
        {
            bowler.TraitCard.IsDead = true;
            DoCardDamage(batsman, bowler);
        }

        if (batsman.TraitCard.Ability == TraitAbility.SWITCH_OWN_CARD && batsman.HitPoint <= 6){
          
            GetComponent<MatchController>().SwitchOwnCard(batsman);
        }
    }

    void DoCardDamage(PlayerCard batsman, PlayerCard bowler)
    {
        Debug.Log("Doing card Damage");

        if (batsman.TraitCard.Ability == TraitAbility.AFFECT_TRAIT)
        {
            AffectTrait(batsman,bowler);
        }

        if (batsman.TraitCard.Ability == TraitAbility.AFFECT_RUN)
        {
            AffectRun(batsman);
        }

        if (batsman.TraitCard.Ability == TraitAbility.AFFECT_ATK)
        {
            Debug.LogError("this is here");
            AffectAtk(batsman,bowler);
        }

        if (batsman.TraitCard.Ability == TraitAbility.AFFECT_HP)
        {
            
            AffectHp(batsman);
        }
        if (batsman.TraitCard.Ability == TraitAbility.COUNTER_ATK)
        {
            AffectCounter(batsman,bowler);
        }
        if (batsman.TraitCard.Ability == TraitAbility.DISABLE_TRAIT){
            bowler.TraitCard.DisableTrait();
        }
        if (batsman.TraitCard.Ability == TraitAbility.NO_DAMAGE){
            Debug.Log("unbreak");
        }

        if (batsman.TraitCard.Ability == TraitAbility.AFFECT_OPPONENT_HP)
        {
            Debug.Log("battle cry");
            AffectOpponentHP(batsman, bowler);
        }

        if (batsman.TraitCard.Ability == TraitAbility.AFFECT_OPPONENT_ATK)
        {
            Debug.Log("intimidate");
            AffectOpponentATK(batsman, bowler);
        }

    }

    private void AffectCounter(PlayerCard batsman, PlayerCard bowler){
        batsman.AttackPoint = bowler.AttackPoint;
    }

    void AffectRun(PlayerCard batsman)
    {
        GameController.Controller().CurrentScore += batsman.TraitCard.AffectedRun;
    }

    void AffectAtk(PlayerCard batsman,PlayerCard bowler)
    {
        Debug.Log("apeffect: " + batsman.TraitCard.ApAffect.ToString());
        batsman.AttackPoint += batsman.TraitCard.ApAffect;
    }

    void AffectHp(PlayerCard batsman)
    {
      
        batsman.HitPoint += batsman.TraitCard.HpAffect;
       
    }

    void AffectTrait(PlayerCard batsman, PlayerCard bowler)
    {
        Debug.LogError("bats attat "+batsman.AttackPoint);
        bowler.HitPoint = bowler.HitPoint - batsman.AttackPoint;
        bowler.TraitCard.DisableTrait();
    }

    void AffectOpponentHP(PlayerCard batsman, PlayerCard bowler)
    {
        Debug.Log("prev: " + bowler.HitPoint);
        bowler.HitPoint -= batsman.TraitCard.HpAffect;
        Debug.Log("after: " + bowler.HitPoint);
    }

    void AffectOpponentATK(PlayerCard batsman, PlayerCard bowler)
    {
        Debug.Log("prev: " + bowler.AttackPoint);
        bowler.AttackPoint -= batsman.TraitCard.ApAffect;
        Debug.Log("after: " + bowler.AttackPoint);
    }

}
