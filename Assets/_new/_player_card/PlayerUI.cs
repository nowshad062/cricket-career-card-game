﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {
    public int playerID;
    public Text name;
    public Text hp;
    public Text ap;
    public Text traitName;

    public void UpdateUI(PlayerCard player){
        this.playerID = player.Id;
        this.name.text = player.PlayerName.ToString();
        this.traitName.text = player.TraitCard.Name;
        this.hp.text = player.HitPoint.ToString();
        this.ap.text = player.AttackPoint.ToString();
    }
}
