﻿using UnityEngine;
using System.Collections;
using CoreData;

public class PlayerCard{
    private int id;
    private int attackPoint;
    private int hitPoint;
    private string playerName;
    private int energyCost;
    private PlayerType playerType;
    TraitCard traitCard;
    private int playerRating;
    private PlayerCategory playerCategory;
    private int itemSlot;
    private int attackPointCopy;

    public int AttackPointCopy
    {
        get { return attackPointCopy; }
        set { attackPointCopy = value; }
    }

    public int ItemSlot{
        get { return itemSlot; }
        set { itemSlot = value; }
    }


    public PlayerCategory PlayerCategory{
        get { return playerCategory; }
        set { playerCategory = value; }
    }


    public int PlayerRating
    {
        get { return playerRating; }
        set { playerRating = value; }
    }

    public int Id{
        get { return id; }
        set { id = value; }
    }
    public int AttackPoint{
        get { return attackPoint; }
        set { attackPoint = value; }
    }
    
    public int HitPoint{
        get { return hitPoint; }
        set { hitPoint = value; }
    }
   
    public string PlayerName{
        get { return playerName; }
        set { playerName = value; }
    }
    
    public int EnergyCost{
        get { return energyCost; }
        set { energyCost = value; }
    }
   
    public PlayerType PlayerType{
        get { return playerType; }
        set { playerType = value; }
    }
   
    public TraitCard TraitCard{
        get { return traitCard; }
        set { traitCard = value; }
    }

    public bool isBowler (){
			if (playerType == PlayerType.PACER || playerType == PlayerType.SPINNER || playerType == PlayerType.MEDIUM_PACER) {
				return true;
			} else
				return false;
    }

    public void RestoreDefault()
    {
        this.AttackPoint = this.AttackPointCopy;
    }

   
}
