﻿using UnityEngine;
using System.Collections.Generic;
using CoreData;

public class TraitCardCollection {

    static List<Trait> traitCards;

    public TraitCardCollection()
    {
        traitCards = new List<Trait>();

    }

    public void AddTraitCard(Trait trait)
    {
        traitCards.Add(trait);
    }

    public static Trait GetTraitCard(TraitType traitType)
    {
        for(int i=0; i<traitCards.Count; i++)
        {
            if (traitCards[i].TraitInfo.name == traitType)
                return traitCards[i];
        }

        Debug.Log("caution!Returning null trait");
        return null;
    }

    
}
