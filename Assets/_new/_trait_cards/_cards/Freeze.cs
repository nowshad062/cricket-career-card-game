﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Freeze : TraitCard {

    public Freeze():base()
    {
        this.Name = "Freeze";
        this.Description = "Freeze";
        this.TraitCheck = TraitType.FREEZE;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ActiveDuration = 3;
        this.Ability = TraitAbility.AFFECT_TRAIT;
    }
}
