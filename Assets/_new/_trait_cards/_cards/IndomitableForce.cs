﻿using UnityEngine;
using System.Collections;
using CoreData;
public class IndomitableForce : TraitCard {
    public IndomitableForce():base()
    {
        this.Name = "Indomitalbe Force";
        this.Description = "Indomitalbe Force";
        this.TraitCheck = TraitType.INDOMITABLE_FORCE;
        this.ActiveDuration = 4;
        this.Ability = TraitAbility.AFFECT_TRAIT;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ActiveDuration = 4;
        this.Ability = TraitAbility.AFFECT_FORCE_DAMAGE;
    }
}
