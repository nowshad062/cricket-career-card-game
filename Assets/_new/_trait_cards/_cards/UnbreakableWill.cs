﻿using UnityEngine;
using System.Collections;
using CoreData;

public class UnbreakableWill : TraitCard{

    public UnbreakableWill():base()
    {
        this.Name = "Unbreakable will";
        this.Description = "Unbreakable will";
        this.TraitCheck = TraitType.UNBREAKABLE_WILL;
        this.ActiveDuration = 6;
       
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ActiveDuration = 3;
        this.Ability = TraitAbility.NO_DAMAGE;
    }
}
