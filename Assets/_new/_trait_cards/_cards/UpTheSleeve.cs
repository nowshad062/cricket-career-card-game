﻿using UnityEngine;
using System.Collections;
using CoreData;
public class UpTheSleeve : TraitCard{
    public UpTheSleeve():base()
    {
        this.Name = "Up the sleeve";
        this.Description = "Up the sleeve";
        this.TraitCheck = TraitType.UP_THE_SLEEVE;
    }

    public override void UpdateData(PlayerCard playerCard)
    {

    }
}
