﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Tag : TraitCard{
    public Tag():base()
    {
        this.Name = "Tag";
        this.Description = "Tag";
        this.TraitCheck = TraitType.TAG;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.Ability = TraitAbility.SWITCH_OWN_CARD;
    }
}
