﻿using UnityEngine;
using System.Collections;
using CoreData;
public class RaiseMorale : TraitCard{
    public RaiseMorale():base()
    {
        this.Name = "Rasie morale";
        this.Description = "Raise morale";
        this.TraitCheck = TraitType.RAISE_MORALE;
        this.AffectedRun = 10;
        this.IsDead = false;
        this.ActiveDuration = 1;
        this.Ability = TraitAbility.AFFECT_RUN;

    }

    public override void UpdateData(PlayerCard playerCard)
    {

    }
}
