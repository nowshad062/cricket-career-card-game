﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Intimidate : TraitCard {

    public Intimidate():base()
    {
        this.Name = "Intimidate";
        this.Description = "Intimidate";
        this.TraitCheck = TraitType.INTIMIDATE;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ApAffect = 1;
        this.Ability = TraitAbility.AFFECT_OPPONENT_ATK;
        this.ActiveDuration = 0;
    }
}
