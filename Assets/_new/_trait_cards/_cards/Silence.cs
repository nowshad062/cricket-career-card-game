﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Silence : TraitCard {

    public Silence():base()
    {
        this.Name = "Silence";
        this.Description = "Silence";
        this.TraitCheck = TraitType.SILENCE;
    }

    public override void UpdateData(PlayerCard playerCard)
    {

    }
}
