﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Enrage : TraitCard {
    public Enrage():base()
    {
        this.Name = "Enrage";
        this.Description = "Enrage";
        this.TraitCheck = TraitType.ENRAGE;
   
    }

    public override void UpdateData(PlayerCard playerCard)
    {
      
        this.ApAffect = 3;
        this.ActiveDuration = 120;
        this.Ability = TraitAbility.AFFECT_ATK;
    }
}
