﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Counter : TraitCard {

    public Counter():base()
    {
        this.Name = "Counter";
        this.Description = "Counter";
        this.TraitCheck = TraitType.COUNTER;
        
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.Ability = TraitAbility.COUNTER_ATK;
        this.ActiveDuration = 3;
    }
}
