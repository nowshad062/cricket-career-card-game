﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Transfer : TraitCard {

    public Transfer():base()
    {
        this.Name = "Transfer";
        this.Description = "Transfer";
        this.TraitCheck = TraitType.TRANSFER;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.Ability = TraitAbility.SWITCH_OPPONENT_CARD;

    }
}
