﻿using UnityEngine;
using System.Collections;
using CoreData;

public class BattleCry :TraitCard {

    public BattleCry():base()
    {
        this.Name = "battle cry";
        this.Description = "battle cry";
        this.TraitCheck = TraitType.BATTLECRY;
       
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.HpAffect = 6;
        this.Ability = TraitAbility.AFFECT_OPPONENT_HP;
        this.ActiveDuration = 0;
    }
}
