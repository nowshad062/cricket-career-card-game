﻿using UnityEngine;
using System.Collections;
using CoreData;
public class NONE : TraitCard
{

    public NONE()
        : base()
    {
        this.Name = "None";
        this.Description = "No Trait";
        this.TraitCheck = TraitType.NONE;

    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ApAffect = 0;
        this.Ability = 0;
        this.ActiveDuration = 0;
    }


}
