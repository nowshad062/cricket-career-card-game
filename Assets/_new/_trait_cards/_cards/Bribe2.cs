﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Bribe2 : TraitCard {

    public Bribe2():base()
    {
        this.Name = "bribe2";
        this.Description = "bribe2";
        this.TraitCheck = TraitType.BRIBE_2;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.Ability = TraitAbility.AFFECT_OPPONENT_ATTACK;
    }
}
