﻿using UnityEngine;
using System.Collections;
using CoreData;
public class MasterBlaster : TraitCard {

    public MasterBlaster():base()
    {
        this.Name = "master blaster";
        this.Description = "master blaster";
        this.TraitCheck = TraitType.MASTER_BLASTER;

    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ApAffect = (playerCard.AttackPoint);
        this.Ability = TraitAbility.AFFECT_ATK;
        this.ActiveDuration = 3;
    }


}
