﻿using UnityEngine;
using System.Collections;
using CoreData;

public class Bribe : TraitCard {

    public Bribe():base()
    {
        this.Name = "bribe";
        this.Description = "bribe";
        this.TraitCheck = TraitType.BRIBE;
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.Ability = TraitAbility.AFFECT_OPPONENT_HP;
    }
}
