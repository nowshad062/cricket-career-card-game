﻿using UnityEngine;
using System.Collections;
using CoreData;
public class Immune : TraitCard {
    public Immune():base()
    {
        this.Name = "Immune";
        this.Description = "Immune";
        this.TraitCheck = TraitType.IMMUNE;
       
    }

    public override void UpdateData(PlayerCard playerCard)
    {
        this.ActiveDuration = 120;
        this.Ability = TraitAbility.DISABLE_TRAIT;
    }
}
