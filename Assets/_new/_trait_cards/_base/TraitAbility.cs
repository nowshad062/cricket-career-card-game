﻿public enum TraitAbility
{
    AFFECT_RUN,
    AFFECT_HP,
    AFFECT_ATK,
    AFFECT_TRAIT,

    AFFECT_OPPONENT_HP,
    AFFECT_OPPONENT_ATTACK,
    AFFECT_FORCE_DAMAGE,

    SUMMON_RANDOM_CARD,
    SWITCH_OPPONENT_CARD,
    SWITCH_OWN_CARD,

    NO_DAMAGE,
    COUNTER_ATK,
    DOUBLE_DAMAGE,
    DISABLE_TRAIT,
    AFFECT_OPPONENT_ATK
}