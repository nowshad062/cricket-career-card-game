﻿using UnityEngine;
using System.Collections;
using CoreData;

public abstract class TraitCard {
    int id;
    string name;
    string description;
    TraitType traitCheck;
    int affectedRun;
    TraitAbility ability;

    public TraitAbility Ability
    {
        get { return ability; }
        set { ability = value; }
    }

    int activeDuration;

    public int ActiveDuration
    {
        get { return activeDuration; }
        set { activeDuration = value; }
    }
    bool isDead;

    public bool IsDead
    {
        get { return isDead; }
        set { isDead = value; }
    }
    int hpAffect;

    public int HpAffect
    {
        get { return hpAffect; }
        set { hpAffect = value; }
    }
    int apAffect;

    public int ApAffect
    {
        get { return apAffect; }
        set { apAffect = value; }
    }
    bool tranferable;

    public bool Tranferable
    {
        get { return tranferable; }
        set { tranferable = value; }
    }
    TraitCard weakness;

    public TraitCard Weakness
    {
        get { return weakness; }
        set { weakness = value; }
    }

    public int AffectedRun
    {
        get { return affectedRun; }
        set { affectedRun = value; }
    }

    public TraitType TraitCheck
    {
        get { return traitCheck; }
        set { traitCheck = value; }
    }

    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
   
   

    public string Description
    {
        get { return description; }
        set { description = value; }
    }


    public void DisableTrait()
    {
        this.affectedRun = 0;
        this.apAffect = 0;
        this.hpAffect = 0;
    }


    public abstract void UpdateData(PlayerCard playerCard);
}
