﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Player/List", order = 1)]
public class PlayerData : ScriptableObject {

    public string objectName = "Player Card Data";
    public string playerName;
	
}
