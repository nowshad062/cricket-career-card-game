﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerManager {

    public List<PlayerCard> allPlayers;

    public PlayerManager()
    {
        allPlayers = new List<PlayerCard>();
    }

    public PlayerCard getPlayer(int id)
    {
        return allPlayers[id];
    }

    public void AddPlayer(PlayerCard player)
    {
        allPlayers.Add(player);
    }

    public int GetPlayerIDToAssign()
    {
        return allPlayers.Count;
    }

}
