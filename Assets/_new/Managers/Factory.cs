﻿using UnityEngine;
using System.Collections.Generic;
using System;
using CoreData;

public class Factory : MonoBehaviour
{
    static PlayerManager playerManager = null;
    static TeamManager teamManager = null;

    public CCGDataBase ccgDatabase;
    public Dictionary<string,TraitInfo> traitInfos;

    void Awake()
    {
        traitInfos = new Dictionary<string, TraitInfo>();
        for (int i = 0; i < ccgDatabase.traitInfos.Count; i++) {

            traitInfos.Add(ccgDatabase.traitInfos[i].name.ToString(),ccgDatabase.traitInfos[i]);
        }
    }

    public static PlayerManager getPlayerManager(){
        if (playerManager == null)
        {
            playerManager = new PlayerManager();
        }
        return playerManager;
    }

    public static TeamManager getTeamrManager()
    {
        if (teamManager == null)
        {
            teamManager = new TeamManager();
        }
        return teamManager;
    }

    public void ReadFromCCGDatabase(){

        for (int i = 0; i < ccgDatabase.playerInfos.Count; i++)
        {
            int random = UnityEngine.Random.Range(0,2);

            if(random==0)
                GenerateCommonCricketer(ccgDatabase.playerInfos[i]);

            if(random==1)
                GenerateRareCricketer(ccgDatabase.playerInfos[i]);

            if(random==2)
                GenerateLegendaryCricketer(ccgDatabase.playerInfos[i]);
        }
     
    }

    public CricketerCard GenerateCommonCricketer(PlayerInfo playerInfo) {

        Debug.Log("Constructing common");
		Trait trait = ConstructCommonTrait (traitInfos [playerInfo.trait.ToString ()]);
		Cricketer cricketer = null;

		if (playerInfo.playerType == PlayerType.PACER) {
			cricketer = ConstructPacer (playerInfo.name,playerInfo.commonAbility);	
		} else if (playerInfo.playerType == PlayerType.SPINNER) {
			cricketer = ConstructSpinner (playerInfo.name,playerInfo.commonAbility);
		} else if (playerInfo.playerType == PlayerType.MEDIUM_PACER) {
			cricketer = ConstructMediumPacer (playerInfo.name, playerInfo.commonAbility);
		} else if (playerInfo.playerType == PlayerType.TECHNICAL) {
			cricketer = ConstructTechnical (playerInfo.name, playerInfo.commonAbility);
		} else if (playerInfo.playerType == PlayerType.AGRESSIVE) {
			cricketer = ConstructAggressive (playerInfo.name, playerInfo.commonAbility);
		} else {
			cricketer = ConstructDefensive (playerInfo.name, playerInfo.commonAbility);
		}

		CricketerCard cricketerCard = new CricketerCard (0,cricketer,trait);
		return cricketerCard;
    }

	public CricketerCard GenerateRareCricketer(PlayerInfo playerInfo) {

        Debug.Log("Constructing rare");
		Trait trait = ConstructRareTrait (traitInfos [playerInfo.trait.ToString ()]);
		Cricketer cricketer = null;

		if (playerInfo.playerType == PlayerType.PACER) {
			cricketer = ConstructPacer (playerInfo.name,playerInfo.rareAbility);	
		} else if (playerInfo.playerType == PlayerType.SPINNER) {
			cricketer = ConstructSpinner (playerInfo.name,playerInfo.rareAbility);
		} else if (playerInfo.playerType == PlayerType.MEDIUM_PACER) {
			cricketer = ConstructMediumPacer (playerInfo.name, playerInfo.rareAbility);
		} else if (playerInfo.playerType == PlayerType.TECHNICAL) {
			cricketer = ConstructTechnical (playerInfo.name, playerInfo.rareAbility);
		} else if (playerInfo.playerType == PlayerType.AGRESSIVE) {
			cricketer = ConstructAggressive (playerInfo.name, playerInfo.rareAbility);
		} else {
			cricketer = ConstructDefensive (playerInfo.name, playerInfo.rareAbility);
		}

		CricketerCard cricketerCard = new CricketerCard (0,cricketer,trait);
		return cricketerCard;
	}

	public CricketerCard GenerateLegendaryCricketer(PlayerInfo playerInfo) {
        Debug.Log("Constructing legendary");
		Trait trait = ConstructLegendaryTrait (traitInfos [playerInfo.trait.ToString ()]);
		Cricketer cricketer = null;

		if (playerInfo.playerType == PlayerType.PACER) {
			cricketer = ConstructPacer (playerInfo.name,playerInfo.legandaryAbility);	
		} else if (playerInfo.playerType == PlayerType.SPINNER) {
			cricketer = ConstructSpinner (playerInfo.name,playerInfo.legandaryAbility);
		} else if (playerInfo.playerType == PlayerType.MEDIUM_PACER) {
			cricketer = ConstructMediumPacer (playerInfo.name, playerInfo.legandaryAbility);
		} else if (playerInfo.playerType == PlayerType.TECHNICAL) {
			cricketer = ConstructTechnical (playerInfo.name, playerInfo.legandaryAbility);
		} else if (playerInfo.playerType == PlayerType.AGRESSIVE) {
			cricketer = ConstructAggressive (playerInfo.name, playerInfo.legandaryAbility);
		} else {
			cricketer = ConstructDefensive (playerInfo.name, playerInfo.legandaryAbility);
		}

		CricketerCard cricketerCard = new CricketerCard (0,cricketer,trait);
		return cricketerCard;
	}

	public Spiner ConstructSpinner(string name,Ability ability){

        return new Spiner(name, ability.attackPoint, ability.healthPoint);
	}

	public Pacer ConstructPacer(string name,Ability ability){
        return new Pacer (name, ability.attackPoint, ability.healthPoint);
	}

	public MediumPacer ConstructMediumPacer(string name,Ability ability){
        return new MediumPacer(name, ability.attackPoint, ability.healthPoint);
	}

	public Aggressive ConstructAggressive(string name,Ability ability){
        return new Aggressive(name, ability.attackPoint, ability.healthPoint);
	}

	public Defensive ConstructDefensive(string name,Ability ability){
        return new Defensive(name, ability.attackPoint, ability.healthPoint);
	}

	public Technical ConstructTechnical(string name,Ability ability){
        return new Technical(name, ability.attackPoint, ability.healthPoint);
	}

    public Trait ConstructCommonTrait(TraitInfo traitInfo) {
        return new Trait(traitInfo.name.ToString(), traitInfo);
    }

    public Trait ConstructRareTrait(TraitInfo traitInfo) {
        return new Trait(traitInfo.name.ToString(), traitInfo);
    }

    public Trait ConstructLegendaryTrait(TraitInfo traitInfo) {
        return new Trait(traitInfo.name.ToString(), traitInfo);
    }

	int GeneratePlayerId(){
		return 5;
	}
}
