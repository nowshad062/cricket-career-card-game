﻿using UnityEngine;
using System.Collections;
using CoreData;

public class PlayerGenerator{

    PlayerManager playerManager;
    //TraitCardCollection traitCards;
    BuffCardCollection buffCards;
    DebuffCardCollection debuffCards;

    public PlayerGenerator()
    {
        playerManager = Factory.getPlayerManager();
        //traitCards = new TraitCardCollection();
        buffCards = new BuffCardCollection();
        debuffCards = new DebuffCardCollection();
    }

    public void CreateCricketer(int Id, string name, int hp, int atk, int energyCost, PlayerType type, PlayerCategory category, TraitType traitName)
    {
        Cricketer cricketer;

        if (type == PlayerType.AGRESSIVE)
            cricketer = new Aggressive(name, atk, hp);

    }

    public void CreatePlayer(int Id, string name, int hp, int atk, int energyCost, PlayerType type, PlayerCategory category, TraitType traitName)
    {
        PlayerCard card = new PlayerCard();
        card.Id = Id;
        card.PlayerName = name;
        card.HitPoint = hp;
        card.AttackPoint = atk;
        card.AttackPointCopy = atk;
        card.EnergyCost = energyCost;
        card.PlayerType = type;
        card.PlayerCategory = category;

//        card.TraitCard = traitCards.GetTraitCard(traitName);
//        card.TraitCard.UpdateData(card);
       
        playerManager.AddPlayer(card);
    }

    public void PrintPlayers()
    {
        for(int i=0; i<playerManager.allPlayers.Count; i++)
            Debug.Log("Player with ID: " + playerManager.allPlayers[i].Id + "::" + "HP" + playerManager.allPlayers[i].HitPoint + ":: AP" + playerManager.allPlayers[i].AttackPoint);
    }

}
