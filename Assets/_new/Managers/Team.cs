﻿using UnityEngine;
using System.Collections.Generic;

public class Team {

    int teamId;
    string countryName;
    string name;
    string shortName;
    List<int> allPlayers;

    List<UtilityCard> buffCards;

    public List<UtilityCard> BuffCards
    {
        get { return buffCards; }
        set { buffCards = value; }
    }
    List<UtilityCard> debuffCards;

    public List<UtilityCard> DebuffCards
    {
        get { return debuffCards; }
        set { debuffCards = value; }
    }


    public Team(int teamId, string name)
    {
        this.teamId = teamId;
        this.name = name;
        allPlayers = new List<int>();
    }


    public int TeamId
    {
        get
        {
            return teamId;
        }
    }

    public string CountryName
    {
        get
        {
            return countryName;
        }
        set
        {
            countryName = value;
        }
    }

    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }

    public string ShortName
    {
        get
        {
            return shortName;
        }
        set
        {
            shortName = value;
        }
    }

    public List<int> AllPlayers
    {
        get
        {
            return allPlayers;
        }
    }

    public void AddPlayerId(int playerId)
    {
        allPlayers.Add(playerId);
    }

}

