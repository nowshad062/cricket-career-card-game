﻿using UnityEngine;
using System.Collections.Generic;

public class TeamGenerator {

    TeamManager teamManager;
    PlayerManager playerManager;

    public TeamGenerator()
    {
        teamManager = Factory.getTeamrManager();
        playerManager = Factory.getPlayerManager();
    }

    public void CreateTeams()
    {
        for (int i = 0; i < 5; i++)
        {
            Team team = new Team(i, "Team " + i.ToString());
            AddPlayersToTeam(team);
            AddBuffCardsToTeam(team);
            AddDebuffCardsToTeam(team);
            teamManager.AddTeam(team);
        }

        //PrintTeams();

    }

    void AddPlayersToTeam(Team team)
    {

        for (int i = (team.TeamId*11) , j = i*3; i < (team.TeamId+1)*11; i++, j = i*3)
        {
            int random = Random.Range(j, j + 2);
            PlayerCard player = playerManager.getPlayer(random);
            team.AddPlayerId(player.Id);
           
            ///Debug.Log("Adding Player no: " + i.ToString() + "  " + player.Id + " " + player.PlayerName + " " + player.PlayerType + " ");
        }

    }

    void AddDebuffCardsToTeam(Team team)
    {
        DebuffCardCollection debuffs = new DebuffCardCollection();
        team.DebuffCards = debuffs.DebuffCards;
    }

    void AddBuffCardsToTeam(Team team)
    {
        BuffCardCollection buffs = new BuffCardCollection();
        team.BuffCards = buffs.BuffCards;
    }

    void PrintTeams()
    {
        for (int i = 0; i < 5; i++)
            Debug.Log(teamManager.getTeam(i).AllPlayers.Count.ToString());
    }

}
