﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TeamManager
{

    List<Team> teamList;

    public TeamManager()
    {
        teamList = new List<Team>();
    }

    public List<Team> TeamList
    {
        get
        {
            return teamList;
        }
    }

    public Team getTeam(int teamId)
    {

        foreach (Team team in teamList)
        {
            if (team.TeamId == teamId)
            {
                return team;
            }
        }

        return null;
    }

    public int GetTeamIdToAssign()
    {
        return teamList.Count;
    }

    public void AddTeam(Team team)
    {
        teamList.Add(team);
    }
}