﻿using UnityEngine;
using System.Collections.Generic;

public class BuffCardCollection {

    List<UtilityCard> buffCards;

    public List<UtilityCard> BuffCards
    {
        get { return buffCards; }
        set { buffCards = value; }
    }

    public BuffCardCollection()
    {
        buffCards = new List<UtilityCard>();
        populateBuffCards();
    }

    void populateBuffCards()
    {
        for (int i = 0; i < 5; i++)
        {
            UtilityCard utilityCard = new UtilityCard();
            utilityCard.name = "Buff " + i.ToString();
            utilityCard.health = Random.Range(10, 30);
            utilityCard.attack = Random.Range(1, 4);
            utilityCard.duration = 6;
            utilityCard.buff = true;
            buffCards.Add(utilityCard);
        }
    }
}
