﻿using UnityEngine;
using System.Collections.Generic;

public class DebuffCardCollection
{
    List<UtilityCard> debuffCards;

    public List<UtilityCard> DebuffCards
    {
        get { return debuffCards; }
        set { debuffCards = value; }
    }

    public DebuffCardCollection()
    {
        debuffCards = new List<UtilityCard>();
        populateDeBuffCards();
    }

    void populateDeBuffCards()
    {
        for (int i = 0; i < 4; i++)
        {
            UtilityCard utilityCard = new UtilityCard();
            utilityCard.name = "Debuff " + i.ToString();
            utilityCard.health = Random.Range(10, 30);
            utilityCard.attack = Random.Range(1, 4);
            utilityCard.duration = 6;
            utilityCard.buff = false;
            debuffCards.Add(utilityCard);
        }
    }
}
