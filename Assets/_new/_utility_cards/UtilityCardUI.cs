﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UtilityCardUI : MonoBehaviour {

    public Text name;
    public Text hp;
    public Text ap;

    public void UpdateUI(UtilityCard buffUtility)
    {
        this.name.text = buffUtility.name.ToString();
        this.hp.text = buffUtility.health.ToString();
        this.ap.text = buffUtility.attack.ToString();
    }
}
