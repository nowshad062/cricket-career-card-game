﻿using UnityEngine;
using System.Collections;

public class UtilityCard {

   public string name;
   public int attack;
   public int duration;
   public int health;
   public bool buff; 

   public void Consume(PlayerCard player)
   {
       if (buff)
       {
           player.HitPoint += this.health;
           player.AttackPoint += this.attack;
           this.duration -= 1;
       }
       else
       {
           player.HitPoint -= this.health;
           player.AttackPoint -= this.attack;
           this.duration -= 1;
       }
   }

}
