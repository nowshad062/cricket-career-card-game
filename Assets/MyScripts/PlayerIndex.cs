﻿using UnityEngine;
using System.Collections;

public class PlayerIndex : MonoBehaviour {

    public int playerIndex;
    public bool buff;

    public int GetPlayerIndex()
    {
        return playerIndex;        
    }
    public void SetPlayerIndex(int value)
    {
        playerIndex = value;        
    }
    public bool GetBuff()
    {
        return buff;
    }
    public void SetBuff(bool value)
    {
        buff = value;
    }

}
