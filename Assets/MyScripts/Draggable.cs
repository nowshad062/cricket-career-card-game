﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    
    public MatchControllerCopy controller;
	public Transform parentToReturnTo ;
	public Transform placeholderParent;
    public ConstraintsScript constraintsPanel;
    GameObject placeholder = null;

    

    public void OnBeginDrag(PointerEventData eventData) {
		//Debug.Log ("OnBeginDrag");
		
		placeholder = new GameObject();
		placeholder.transform.SetParent( this.transform.parent );
     

        placeholder.transform.SetSiblingIndex( this.transform.GetSiblingIndex() );
		
		parentToReturnTo = this.transform.parent;
		placeholderParent = parentToReturnTo;
		this.transform.SetParent( this.transform.parent.parent );
		
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	
	public void OnDrag(PointerEventData eventData) { 
		//Debug.Log ("OnDrag");
		
		this.transform.position = eventData.position;

		if(placeholder.transform.parent != placeholderParent)
			placeholder.transform.SetParent(placeholderParent);

		int newSiblingIndex = placeholderParent.childCount;

		for(int i=0; i < placeholderParent.childCount; i++) {
			if(this.transform.position.x < placeholderParent.GetChild(i).position.x) {

				newSiblingIndex = i;

				if(placeholder.transform.GetSiblingIndex() < newSiblingIndex)
					newSiblingIndex--;

				break;
			}
		}

		placeholder.transform.SetSiblingIndex(newSiblingIndex);
        if (constraintsPanel != null)
        {
            constraintsPanel.Constraints(parentToReturnTo);
        }
	}
	
	public void OnEndDrag(PointerEventData eventData) {
		//Debug.Log ("OnEndDrag");
		this.transform.SetParent( parentToReturnTo );
		this.transform.SetSiblingIndex( 0);
		GetComponent<CanvasGroup>().blocksRaycasts = true;
      
        if (controller.GetBatting())
        {
            constraintsPanel.UpdateConstraintsPanels(false, true, false, true, false, false, false, false);
        }else
            constraintsPanel.UpdateConstraintsPanels(true, false, true, false, false, false, false, false);

        Destroy(placeholder);
	}
	
	
	
}
