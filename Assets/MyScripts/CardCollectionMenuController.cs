﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CardCollectionMenuController : MonoBehaviour {

    List<PlayerCard> battingCardCollection;
    List<PlayerCard> bowlingCardCollection;
    public Transform list1;
    public Transform list2;
    Transform[] batsman1;
    Transform[] batsman2;
    //Transform[] bowler1;
    //Transform[] bowler2;

    void Start () {
        batsman1 = new Transform[10];
        batsman2 = new Transform[10];
        battingCardCollection = GetComponent<PopulateCard>().GetBattingTeam();
        bowlingCardCollection = GetComponent<PopulateCard>().GetBowlingTeam();
        PlotTheValuesIntheCards();
	}
	
	void Update () {
	
	}
    void PlotTheValuesIntheCards()
    {
        for (int i = 0; i < 10; i++)
        {
            batsman1[i] = list1.GetChild(i).GetComponent<Transform>();
            batsman2[i] = list2.GetChild(i).GetComponent<Transform>();

            list1.GetChild(i).GetComponent<PlayerIndex>().SetPlayerIndex(i);
            list2.GetChild(i).GetComponent<PlayerIndex>().SetPlayerIndex(i + 10);
        }
        UpdateBattingCards();
    }

    void UpdateBattingCards()
    {
        for (int i = 0; i < 10; i++)
        {
            batsman1[i].GetChild(0).GetComponent<Text>().text = battingCardCollection[i].HitPoint.ToString();
            batsman1[i].GetChild(1).GetComponent<Text>().text = battingCardCollection[i].AttackPoint.ToString();

        }
        for (int i = 0; i < 10; i++)
        {
            batsman2[i].GetChild(0).GetComponent<Text>().text = battingCardCollection[i + 10].HitPoint.ToString();
            batsman2[i].GetChild(1).GetComponent<Text>().text = battingCardCollection[i + 10].AttackPoint.ToString();
        }
    }

}
