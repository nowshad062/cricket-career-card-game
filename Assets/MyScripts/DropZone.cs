﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {

    public MatchControllerCopy controller;
    

    public Transform battingSlot;
    public Transform bowlingSlot;

    void Awake()
    {
       
    }
   

	public void OnPointerEnter(PointerEventData eventData) {
		//Debug.Log("OnPointerEnter");
		if(eventData.pointerDrag == null)
			return;

		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null) {
			d.placeholderParent = this.transform;
		}
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		//Debug.Log("OnPointerExit");
		if(eventData.pointerDrag == null)
			return;

		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null && d.placeholderParent==this.transform) {
			d.placeholderParent = d.parentToReturnTo;
		}
	}
	
	public void OnDrop(PointerEventData eventData) {
		//Debug.Log (eventData.pointerDrag.name + " was dropped on " + gameObject.name);

		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null) {
			d.parentToReturnTo = this.transform;
            d.gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
            d.gameObject.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
            d.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

            //Debug.Log(eventData.pointerDrag.transform.GetComponent<PlayerIndex>().playerIndex.ToString());

            int droppedPlayerIndex = eventData.pointerDrag.transform.GetComponent<PlayerIndex>().playerIndex;
            StartCoroutine(controller.NewCardPlayed(droppedPlayerIndex));

		}

	}

}
