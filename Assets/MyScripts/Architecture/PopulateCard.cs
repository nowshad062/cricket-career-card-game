﻿using UnityEngine;
using System.Collections.Generic;

public class PopulateCard : MonoBehaviour {


    PlayerCard card;
    List<PlayerCard> battingCard;
    List<PlayerCard> bowlingCard;

    UtilityCard utilityCard;
    List<UtilityCard> buff;
    List<UtilityCard> deBuff;
    TraitCardCollection traitCards;

    PlayerManager playerManager;

    void Awake () {
        battingCard = new List<PlayerCard>();
        bowlingCard = new List<PlayerCard>();
        buff = new List<UtilityCard>();
        deBuff = new List<UtilityCard>();
        traitCards = new TraitCardCollection();
        playerManager = Factory.getPlayerManager();


        //PopulateBattingTeam();
        //PopulateBowlingTeam();
        PopulateBuff();
        PopulateDeBuff();
        
    }
    void Start()
    {
        
    }

   /* void PopulateBattingTeam() {
        for (int i = 0; i < 20; i++)
        {
            card = new PlayerCard();
            card.PlayerName = "Player " + i.ToString();
            card.AttackPoint = Random.Range(1, 6);
            card.HitPoint = Random.Range(6, 60);
            card.PlayerRating = Random.Range(1, 4);
            card.PlayerType = (PlayerType)Random.Range(0, 4);
            card.EnergyCost = Random.Range(1, 10);
            card.PlayerCategory = (PlayerCategory)Random.Range(0, 3);
            card.TraitCard = traitCards.GetTraitCard();
            card.TraitCard.UpdateData(card);
            battingCard.Add(card);
        }
    }
    void PopulateBowlingTeam() {
        for (int i = 0; i < 20; i++)
        {
            card = new PlayerCard();
            card.AttackPoint = Random.Range(1, 6);
            card.HitPoint = Random.Range(6, 60);
            card.PlayerRating = Random.Range(1, 4);
            card.PlayerType = (PlayerType)Random.Range(0, 4);
            card.EnergyCost = Random.Range(1, 10);
            card.PlayerCategory = (PlayerCategory)Random.Range(0, 3);
            card.TraitCard = traitCards.GetTraitCard();
            bowlingCard.Add(card);
        }
    }
    */ 
    public List<PlayerCard> GetBattingTeam(){
        return battingCard;
    }
    public List<PlayerCard> GetBowlingTeam()
    {
        return bowlingCard;
    }

    public List<PlayerCard> getBattingBestEleven(){
        List<PlayerCard> temp = new List<PlayerCard>();
        for (int i = 0; i < 11; i++){
            temp.Add(battingCard[i]);
        }
        return temp;
    }
    public List<PlayerCard> getBowlingBestEleven(){
        List<PlayerCard> temp = new List<PlayerCard>();
        for (int i = 0; i < 11; i++){
            temp.Add(bowlingCard[i]);
        }
        return temp;
    }

    void PopulateBuff()
    {
        for (int i = 0; i < 4; i++)
        {
            utilityCard = new UtilityCard();
            utilityCard.health = Random.Range(10, 30);
            utilityCard.attack = Random.Range(1, 4);
            utilityCard.duration = Random.Range(6, 18);
            utilityCard.buff = true;
            buff.Add(utilityCard);
        }

    }
    void PopulateDeBuff()
    {
        for (int i = 0; i < 4; i++)
        {
            utilityCard = new UtilityCard();
            utilityCard.health = Random.Range(10, 30);
            utilityCard.attack = Random.Range(1, 4);
            utilityCard.duration = Random.Range(6, 18);
            utilityCard.buff = false;
            deBuff.Add(utilityCard);
        }
    }
    public List<UtilityCard> GetBuffList() {
        return buff;
    }
    public List<UtilityCard> GetDeBuffList() {
        return deBuff;
    }
}
