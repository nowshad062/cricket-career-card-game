﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using CoreData;

public class MatchController : MonoBehaviour
{

    public Button play;
    public Button simulate;
    public Button selectBatting;
    public Button selectBowling;
    public Button restart;
    public ConstraintsScript constraintsPanel;
    bool batting;
    public Text totalRunText;
    public Text score;
    PopulateCard populateCard;
    List<PlayerCard> battingList;
    List<int> cloneAtks;

    List<PlayerCard> bowlingList;
    List<PlayerCard> battingListEleven;
    List<PlayerCard> bowlingListEleven;
    List<UtilityCard> buffList;
    List<UtilityCard> deBuffList;
    //int totalRun;
    int batsman;
    int bowler;
    int ball;
    int ballDuration;
    bool over = true;
    bool batBuffUsing = true;
    bool ballBuffUsing = true;
    public Transform utilityPanel;
    public Transform battingPanel;
    public Transform bowlingPanel;
    public Transform battingSlot;
    public Transform bowlingSlot;
    public Transform battingBuffSlot;
    public Transform bowlingBuffSlot;
    public Transform matchEndPanel;
    public Transform sideSelectionPanel;
    Transform[] battingButton;
    Transform[] bowlingButton;
    Transform[] buffButton;
    Transform[] deBuffButton;
    Text[] runPerBall;
    public Transform runPerBallPanel;
    public Text ballCounter;
    public Text targetText;
    public Text resultText;
    int target;
    int wicketleft;
    GameController gameController;
    int run = -10;

    Simulation simulation;
   

    void Start()
    {
        batsman = 0;
        bowler = 0;
        runPerBall = new Text[6];
        gameController = GameController.Controller();
        battingButton = new Transform[11];
        bowlingButton = new Transform[11];
        buffButton = new Transform[4];
        deBuffButton = new Transform[4];
        populateCard = GetComponent<PopulateCard>();
        play.onClick.AddListener(delegate {
            
            StartCoroutine(GameCalculation()); StartCoroutine(UnclickableButton()); });
        simulate.onClick.AddListener(delegate { StartCoroutine(SimulateRestOfMatch());  });
        selectBatting.onClick.AddListener(delegate { batting = true; sideSelectionPanel.gameObject.SetActive(false); SelectBatting(); });
        selectBowling.onClick.AddListener(delegate { batting = false; sideSelectionPanel.gameObject.SetActive(false); SelectBowling(); });
        restart.onClick.AddListener(delegate { Restart(); });
        battingList = new List<PlayerCard>();
        bowlingList = new List<PlayerCard>();

        battingList = populateCard.GetBattingTeam();

        cloneAtks = new List<int>();

        for (int i = 0; i < battingList.Count; i++)
        {
            cloneAtks.Add(battingList[i].AttackPoint);
        }


        bowlingList = populateCard.GetBowlingTeam();
        buffList = populateCard.GetBuffList();
        deBuffList = populateCard.GetDeBuffList();
        battingListEleven = new List<PlayerCard>();

        

        bowlingListEleven = new List<PlayerCard>();

        battingListEleven = populateCard.getBattingBestEleven();
        bowlingListEleven = populateCard.getBowlingBestEleven();

        populatebuttons();
        InitSecondInnings();
        wicketleft = battingSlot.childCount-1;
        simulation = GetComponent<Simulation>();

      
    }

    void Update()
    {
        if (battingSlot.childCount > 0 && bowlingSlot.childCount > 0 && battingSlot.GetChild(0).gameObject.activeInHierarchy == true && bowlingSlot.GetChild(0).gameObject.activeInHierarchy == true)
        {
            play.gameObject.SetActive(true);
        }
        else
            play.gameObject.SetActive(false);

        //batting selected drag and drop
        if (battingSlot.childCount > 0 && batting && battingSlot.GetChild(0).GetComponent<PlayerIndex>() != null)
        {
            if (bowlingSlot.childCount == 0)
            {
                bowlingPanel.GetChild(0).SetParent(bowlingSlot);
               
            }
            else if (bowlingPanel.childCount > 0 && bowlingSlot.GetChild(0).gameObject.activeInHierarchy == false)
            {
                bowlingPanel.GetChild(0).SetParent(bowlingSlot);
                int sibInd = bowlingSlot.childCount - 1;
                bowlingSlot.GetChild(sibInd).SetSiblingIndex(0);
            }
          
        }
        //bowling selected drag and drop
        if (bowlingSlot.childCount > 0 && !batting && bowlingSlot.GetChild(0).GetComponent<PlayerIndex>() != null)
        {
            if (battingSlot.childCount == 0)
            {
                battingPanel.GetChild(0).SetParent(battingSlot);
            }
            else if (battingPanel.childCount > 0 && battingSlot.GetChild(0).gameObject.activeInHierarchy == false)
            {
                battingPanel.GetChild(0).SetParent(battingSlot);
                int sibInd = battingSlot.childCount - 1;
                battingSlot.GetChild(sibInd).SetSiblingIndex(0);
            }
        }
    }

    public List<PlayerCard> GetBattingListEleven()
    {
            return battingListEleven;
    }
    public List<PlayerCard> GetBowlingListEleven()
    {
        return bowlingListEleven;
    }

    void populatebuttons()
    {
        for (int i = 0; i < 11; i++)  //Populating the player cards 
        {
            battingButton[i] = battingPanel.GetChild(i).GetComponent<Transform>();
            bowlingButton[i] = bowlingPanel.GetChild(i).GetComponent<Transform>();

            battingPanel.GetChild(i).GetComponent<PlayerIndex>().SetPlayerIndex(i);
            bowlingPanel.GetChild(i).GetComponent<PlayerIndex>().SetPlayerIndex(i);
        }
        for (int i = 0; i < 4; i++)  //populating the player buffs
        {
            buffButton[i] = utilityPanel.GetChild(0).GetChild(i).GetComponent<Transform>();
            deBuffButton[i] = utilityPanel.GetChild(1).GetChild(i).GetComponent<Transform>();

            utilityPanel.GetChild(0).GetChild(i).GetComponent<PlayerIndex>().SetPlayerIndex(i);
            utilityPanel.GetChild(1).GetChild(i).GetComponent<PlayerIndex>().SetPlayerIndex(i);
            utilityPanel.GetChild(0).GetChild(i).GetComponent<PlayerIndex>().SetBuff(true);
            utilityPanel.GetChild(1).GetChild(i).GetComponent<PlayerIndex>().SetBuff(false);
        }
        for (int i = 0; i < 6; i++){
            runPerBall[i] = runPerBallPanel.GetChild(i).GetComponent<Text>();
        }
        UpdateBattingCards();
        UpdateBowlingCards();
        UpdateBuffList();
        UpdateDeBuffList();
    }
    void UpdateBattingCards()
    {
        for (int i = 0; i < battingButton.Length; i++)
        {
            
            battingButton[i].GetChild(0).GetComponent<Text>().text = battingListEleven[i].HitPoint.ToString();
            battingButton[i].GetChild(1).GetComponent<Text>().text = battingListEleven[i].AttackPoint.ToString();
            battingButton[i].GetChild(4).GetComponent<Text>().text = battingListEleven[i].TraitCard.ToString();
           

        }
    }
    void UpdateBowlingCards()
    {
        for (int i = 0; i < bowlingButton.Length; i++)
        {
            
            bowlingButton[i].GetChild(0).GetComponent<Text>().text = bowlingListEleven[i].HitPoint.ToString();
            bowlingButton[i].GetChild(1).GetComponent<Text>().text = bowlingListEleven[i].AttackPoint.ToString();
            bowlingButton[i].GetChild(2).GetComponent<Text>().text = bowlingListEleven[i].TraitCard.ToString();
        }
    }
    void UpdateBuffList() {
        for (int i = 0; i < 4; i++)
        {
            buffButton[i].GetChild(0).GetComponent<Text>().text = buffList[i].health +"\n    "+buffList[i].attack;
        }
    }
    void UpdateDeBuffList() {
        for (int i = 0; i < 4; i++)
        {
            deBuffButton[i].GetChild(0).GetComponent<Text>().text = deBuffList[i].health + "\n     " + deBuffList[i].attack;
        }
    }

   
    void SelectBestEleven()
    {
        for (int i = 0; i < 11; i++)
        {
            battingListEleven.Add(battingList[i]);
            bowlingListEleven.Add(bowlingList[i]);
        }
    }
    void RunPerBallCalculation(int run, bool wicketDown)
    {
        
        for (int i = 0; i < 5; i++)
        {
            runPerBall[i].text = runPerBall[i + 1].text;
            if (runPerBall[i + 1].text == "w")
            {
                runPerBall[i].color = Color.red;
            }
            else {
                runPerBall[i].color = Color.black;
            }
        }
        if (!wicketDown)
        {
            runPerBall[5].text = run.ToString();
            runPerBall[5].color = Color.black;
        }
        else {
            runPerBall[5].text = "W";
            runPerBall[5].color = Color.red;
        }
    }
    bool WicketDown()
    {
        if (wicketleft > battingSlot.childCount)
        {
            wicketleft = battingSlot.childCount;
            return true;            
        }else
            return false;
    }
    
    
    void GetPlayerIndex()
    {
        batsman = battingSlot.GetChild(0).GetComponent<PlayerIndex>().GetPlayerIndex();
        bowler = bowlingSlot.GetChild(0).GetComponent<PlayerIndex>().GetPlayerIndex();
    }
    void BatsmanBuffSlotCalculationHealth(){
        if (battingBuffSlot.GetChild(0).gameObject.activeInHierarchy == true && batBuffUsing){
            //Debug.Log("entered batBuffCalHealth");
            int index = battingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetPlayerIndex();
            if (battingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetBuff()){
                battingListEleven[batsman].HitPoint += buffList[index].health;
                //Debug.Log("bat HP buff: " + battingListEleven[batsman].healthPoint);
            }
            else{
                bowlingListEleven[bowler].HitPoint -= deBuffList[index].health;
               //Debug.Log("bat AP buff: " + bowlingListEleven[bowler].healthPoint);
            }
            
        }
    }
    void BatsmanBuffSlotCalculationAttack()
    {
        if (battingBuffSlot.GetChild(0).gameObject.activeInHierarchy == true && batBuffUsing)
        {
            //Debug.Log("entered batBuff calAP");
            int index = battingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetPlayerIndex();
            if (battingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetBuff())
            {
                battingListEleven[batsman].AttackPoint += buffList[index].attack;
                //Debug.Log("bat AP buff: " + battingListEleven[batsman].attackPoint);
            }
            else {
                bowlingListEleven[bowler].AttackPoint -= deBuffList[index].attack;
            }
            batBuffUsing = false;
        }
    }
    void BowlerBuffSlotCalculationHealth(){

        if (bowlingBuffSlot.GetChild(0).gameObject.activeInHierarchy == true && ballBuffUsing)
        {
            int index = bowlingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetPlayerIndex();
            if (bowlingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetBuff()){
                bowlingListEleven[bowler].HitPoint += buffList[index].health;
                
            }
            else {
                battingListEleven[batsman].HitPoint -= deBuffList[index].health;
            }
            
        }
    }
    void BowlerBuffSlotCalculationAttack(){
        if (bowlingBuffSlot.GetChild(0).gameObject.activeInHierarchy == true && ballBuffUsing){
            int index = bowlingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetPlayerIndex();
            if (bowlingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetBuff())
            {
                bowlingListEleven[bowler].AttackPoint += buffList[index].attack;
                ballDuration = ball + buffList[index].duration;
            }
            else {
                battingListEleven[batsman].AttackPoint -= deBuffList[index].attack;
                ballDuration = ball + deBuffList[index].duration;
            }
            ballBuffUsing = false;
            Debug.Log("Ballduration: "+ballDuration);
        }
        
    }

    void UpdatePlayerHP()
    {

       

        //battingSlot.GetChild(0).GetChild(0).GetComponent<Text>().text = battingListEleven[batsman].HitPoint.ToString();
        //bowlingSlot.GetChild(0).GetChild(0).GetComponent<Text>().text = bowlingListEleven[bowler].HitPoint.ToString();


        battingButton[batsman].GetComponent<PlayerUI>().UpdateUI(battingListEleven[batsman]);
        bowlingButton[bowler].GetComponent<PlayerUI>().UpdateUI(bowlingListEleven[bowler]);

       // battingSlot.GetChild(0).GetComponent<PlayerUI>().UpdateUI(battingListEleven[batsman]);
       // bowlingSlot.GetChild(0).GetComponent<PlayerUI>().UpdateUI(bowlingListEleven[bowler]);
    }
    
    IEnumerator GameCalculation(){
        GetPlayerIndex();
            
        BatsmanBuffSlotCalculationHealth();     //Keep specificly in this order
        BatsmanBuffSlotCalculationAttack();     //Keep specificly in this order
        BowlerBuffSlotCalculationHealth();      //Keep specificly in this order
        BowlerBuffSlotCalculationAttack();      //Keep specificly in this order
        //TypeMatchUpCalculation();


        yield return new WaitForEndOfFrame();
        yield return new WaitForFixedUpdate();

        PlayerCard playerBatsman = battingListEleven[batsman];
        PlayerCard playerBowler = bowlingListEleven[bowler];

       // while (battingListEleven[batsman].HitPoint > 0 && bowlingListEleven[bowler].HitPoint > 0 && BallCounter())
        {

            /*simulation.Simulate(playerBatsman, playerBowler);
            int runSCoredFromSim = simulation.RunScored;
            UpdatePlayerHP();


            run = RunLogic(battingListEleven[batsman].AttackPoint, run);
            run += simulation.RunScored;


            gameController.CurrentScore += run;
            ball++;
            RunPerBallCalculation(run, WicketDown());
            totalRunText.text = gameController.CurrentScore.ToString();
            score.text = run.ToString();
            score.GetComponent<Animator>().SetTrigger("ShowScore");
            ballCounter.text = (int)(ball / 6) + "." + ball % 6 + " Ov";

            if ((ball % 6) == 0 && bowlingSlot.GetChild(0).gameObject.activeInHierarchy == true)
            {
                bowlingSlot.GetChild(0).SetParent(bowlingPanel);
            }*/

           
            //SimulateDyingWish(battingListEleven[batsman], bowlingListEleven[bowler]);
           // battingListEleven[batsman].HitPoint = battingListEleven[batsman].HitPoint - bowlingListEleven[bowler].AttackPoint;



            Debug.Log("Doing batting" + battingListEleven[batsman].TraitCard.Name);
            simulation.Simulate(playerBatsman, playerBowler);
           
            //playerBatsman.AttackPoint = battingListEleven[batsman].AttackPoint;

            int runSCoredFromSim = simulation.RunScored;

            if (battingListEleven[batsman].HitPoint < 1)
            {
                battingButton[batsman].gameObject.SetActive(false);
                battingBuffSlot.GetChild(0).gameObject.SetActive(false);
                batBuffUsing = true;
              
                simulation.BallPassed = 0;
            }
           // bowlingListEleven[bowler].HitPoint = bowlingListEleven[bowler].HitPoint - battingListEleven[batsman].AttackPoint;

            if (bowlingListEleven[bowler].HitPoint < 1)
            {
                bowlingButton[bowler].gameObject.SetActive(false);
            }
            if (ballDuration<ball)// ball buff slot depends on duration of balls
            {
                bowlingBuffSlot.GetChild(0).gameObject.SetActive(false);
                ballBuffUsing = true;
            }
          
            run = RunLogic(battingListEleven[batsman].AttackPoint, run);
            run += runSCoredFromSim;

            gameController.CurrentScore += run;
            ball++;
            RunPerBallCalculation(run, WicketDown());
            totalRunText.text = gameController.CurrentScore.ToString();
            score.text = run.ToString();
            score.GetComponent<Animator>().SetTrigger("ShowScore");
            ballCounter.text = (int)(ball / 6) + "." + ball % 6 + " Ov";

            UpdatePlayerHP();

           // PrintClone();
            Debug.Log("Resetting attack point to" + cloneAtks[batsman]);
            playerBatsman.AttackPoint = cloneAtks[batsman];

            if ((ball%6)==0 && bowlingSlot.GetChild(0).gameObject.activeInHierarchy == true){
                bowlingSlot.GetChild(0).SetParent(bowlingPanel);
            }

            //PlayRestOfMatch();
        yield return new WaitForSeconds(0.5f);

          /*  if (gameController.GetSecondInnings())
            {
                if (totalRun > target)
                {
                    InningsEnd();
                    break;
                }else if (ball >= 120)
                {
                    InningsEnd();
                    break;
                }
                else if (battingPanel.childCount < 1 && battingSlot.GetChild(0).gameObject.activeInHierarchy == false)
                {
                    InningsEnd();
                    break;
                }
            }
            else{
                if (ball >= 120){
                    InningsEnd();
                    break;
                }
                else if (battingPanel.childCount < 1 && battingSlot.GetChild(0).gameObject.activeInHierarchy == false){
                    InningsEnd();
                    break;
                }
            }*/
        }
    }

    void PrintClone()
    {
        for (int i = 0; i < cloneAtks.Count; i++)
        {
            Debug.Log(cloneAtks[i]);
        }
        
    }

    int RunLogic(int batsmanAP, int prevRun)
    {
        int[] limit = new int[7];
        switch (batsmanAP)
        {
            case 1:
                limit[0] = 50;
                limit[1] = 70;
                limit[2] = 80;
                limit[3] = 90;
                limit[4] = 99;
                limit[5] = 0;
                limit[6] = 1;
                break;
            case 2:
                limit[0] = 45;
                limit[1] = 70;
                limit[2] = 80;
                limit[3] = 90;
                limit[4] = 98;
                limit[5] = 0;
                limit[6] = 2;
                break;
            case 3:
                limit[0] = 40;
                limit[1] = 65;
                limit[2] = 80;
                limit[3] = 90;
                limit[4] = 97;
                limit[5] = 0;
                limit[6] = 3;
                break;
            case 4:
                limit[0] = 35;
                limit[1] = 60;
                limit[2] = 75;
                limit[3] = 90;
                limit[4] = 96;
                limit[5] = 0;
                limit[6] = 4;
                break;
            case 5:
                limit[0] = 30;
                limit[1] = 55;
                limit[2] = 75;
                limit[3] = 90;
                limit[4] = 95;
                limit[5] = 0;
                limit[6] = 5;
                break;
            case 6:
                limit[0] = 25;
                limit[1] = 45;
                limit[2] = 70;
                limit[3] = 85;
                limit[4] = 94;
                limit[5] = 0;
                limit[6] = 6;
                break;
            case 7:
                limit[0] = 20;
                limit[1] = 35;
                limit[2] = 60;
                limit[3] = 80;
                limit[4] = 93;
                limit[5] = 0;
                limit[6] = 7;
                break;
            case 8:
                limit[0] = 15;
                limit[1] = 25;
                limit[2] = 50;
                limit[3] = 75;
                limit[4] = 92;
                limit[5] = 0;
                limit[6] = 8;
                break;
            case 9:
                limit[0] = 10;
                limit[1] = 15;
                limit[2] = 40;
                limit[3] = 65;
                limit[4] = 81;
                limit[5] = 0;
                limit[6] = 9;
                break;
            case 10:
                limit[0] = 5;
                limit[1] = 10;
                limit[2] = 35;
                limit[3] = 60;
                limit[4] = 90;
                limit[5] = 0;
                limit[6] = 10;
                break;
        }


        if(prevRun == -10)
            return CalculateRun(limit);

        for (int i = 0; i < 7; i++)
            limit[i] = (limit[i] + 1);

        Debug.Log("prev run: " + prevRun);

        limit[prevRun] = limit[prevRun] - 6;
        
        return CalculateRun(limit);


    } 



    int CalculateRun(int[] limits)
    {
        int x = Random.Range(0, 100);
        if (x < limits[0] && x > 0){
            return 0;
        }else if (x < limits[1] && x > limits[0])
        {
            return 1;
        }else if (x < limits[2] && x > limits[1])
        {
            return 2;
        }else if (x < limits[3] && x > limits[2])
        {
            return 3;
        }else if (x < limits[4] && x > limits[3])
        {
            return 4;
        }else{
            return 6;
        }

    }

    void HalfHP(PlayerCard player){
        player.HitPoint /= 2;
    }

    void TypeMatchUpCalculation(){

        Debug.Log(battingListEleven[batsman].PlayerType.ToString());
        Debug.Log(bowlingListEleven[bowler].PlayerType.ToString());

        PlayerType batsmanType = battingListEleven[batsman].PlayerType;
        PlayerType bowlerType = bowlingListEleven[bowler].PlayerType;

        TypeMatch match = new TypeMatch();

        Debug.Log("battingListEleven[batsman].AttackPoint before: " + battingListEleven[batsman].AttackPoint);
        Debug.Log("bowlingListEleven[batsman].AttackPoint before: " + bowlingListEleven[bowler].AttackPoint);

        battingListEleven[batsman].AttackPoint += match.getBattingAttackIncrease(batsmanType,bowlerType);
        bowlingListEleven[bowler].AttackPoint += match.getBowlingAttackIncrease(batsmanType, bowlerType);

        Debug.Log("battingListEleven[batsman].AttackPoint after: " + battingListEleven[batsman].AttackPoint);
        Debug.Log("bowlingListEleven[batsman].AttackPoint afetr: " + bowlingListEleven[bowler].AttackPoint);



        /* if (battingBuffSlot.GetChild(0).GetComponent<PlayerIndex>().GetBuff()){

                battinglist
                .HitPoint += buffList[index].health;
                //Debug.Log("bat HP buff: " + battingListEleven[batsman].healthPoint);
            }
            else{
                bowlingListEleven[bowler].HitPoint -= deBuffList[index].health;
               //Debug.Log("bat AP buff: " + bowlingListEleven[bowler].healthPoint);
            }*/

    }


    bool BallCounter()
    {
        if (ball % 6 == 0 && ball!=0 && over){
            Debug.Log("Over ended");
            this.run = -10;
            over = false;
            return false;
        }
        else {
            over = true;
           
            play.gameObject.SetActive(true);
            return true;
        }
    }
    void InningsEnd()
    {
        if (!gameController.GetSecondInnings()){ // starting second innings
            gameController.SetIsSecondInnings(true);
            gameController.SetBatting(batting);
            gameController.SetTarget(gameController.CurrentScore);
            
        }
        else {
            if (gameController.GetIsBatting())
            {//match end logic
                if (target > gameController.CurrentScore)
                {//player win
                    resultText.text = "YOU WIN";
                    matchEndPanel.GetChild(2).gameObject.SetActive(true);
                }
                else
                { //player lose
                    resultText.text = "YOU LOSE";
                    matchEndPanel.GetChild(2).gameObject.SetActive(true);
                }

                }else{
                    if (target > gameController.CurrentScore)
                {
                    //player lose
                    resultText.text = "YOU LOSE";
                    matchEndPanel.GetChild(2).gameObject.SetActive(true);
                }
                else
                    { //player win
                    resultText.text = "YOU WIN";
                    matchEndPanel.GetChild(2).gameObject.SetActive(true);
                }

                }
        }

               

        matchEndPanel.gameObject.SetActive(true);
    }
    void PlayRestOfMatch() {
        if ((battingPanel.childCount > 0 || battingSlot.GetChild(0).gameObject.activeInHierarchy == true) && bowlingPanel.childCount<1 && bowlingSlot.GetChild(0).gameObject.activeInHierarchy==false)
        {
            simulate.gameObject.SetActive(true);
        }
    }
    IEnumerator SimulateRestOfMatch(){
        int run = -10;
        while (ball<120){
            ball++;
            run = RunLogic(battingListEleven[batsman].AttackPoint,run);
            gameController.CurrentScore += run;
            totalRunText.text = gameController.CurrentScore.ToString();
            score.text = run.ToString();
            ballCounter.text = (int)(ball / 6) + "." + ball % 6 + " Ov";
            yield return new WaitForSeconds(0.2f);
            if (ball==120)
            {
                InningsEnd();
                break;
            }
        }
    }
    void SelectBatting()
    {
        constraintsPanel.UpdateConstraintsPanels(false, true, false, true, false, true, false, false);
    }
      void SelectBowling(){
            constraintsPanel.UpdateConstraintsPanels(true, false, true, false, true, false, false, false);
    }

    public bool GetBatting(){
        return batting;
    }
    public void Restart()
    {
        SceneManager.LoadScene("Arena");
    }
    void InitSecondInnings()
    {
        if (gameController.GetSecondInnings())
        {
            sideSelectionPanel.gameObject.SetActive(false);
            batting = !gameController.GetIsBatting();
            target = gameController.GetTarget();
            targetText.text = "Target: " + target;
            if (gameController.GetIsBatting())
            {
                SelectBowling();
            }
            else
                SelectBatting();
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    IEnumerator UnclickableButton()
    {
        play.interactable = false;
        yield return new WaitForSeconds(2);
        play.interactable = true;
    }


    public IEnumerator NewCardPlayed(int droppedIndex)
    {
        Debug.Log("New card played");

        yield return new WaitForEndOfFrame();

        if (batting)
        {
            //Debug.Log("In batsman New Card Played having trait: " + battingListEleven[droppedIndex].TraitCard.ToString());
            
            PlayerCard batsman = battingListEleven[droppedIndex];
            if (batsman.isBowler())
            {
                Debug.Log("HP become half becuase the batsman is actually a bowler");
                HalfHP(batsman);
                battingButton[droppedIndex].GetComponent<PlayerUI>().UpdateUI(batsman);
            }
        }
        else
        {
           
            PlayerCard bowler = bowlingListEleven[droppedIndex];
            if (!bowler.isBowler())
            {
                Debug.Log("HP become half becuase the bowler is actually a batsman");
                HalfHP(bowler);
                bowlingButton[droppedIndex].GetComponent<PlayerUI>().UpdateUI(bowler);
            }
        }

        GetPlayerIndex();
        simulation.SimulateOnce(battingListEleven[this.batsman], bowlingListEleven[this.bowler]);
        UpdatePlayerHP();


        yield return new WaitForSeconds(2f);
        if (battingListEleven[droppedIndex].TraitCard.Ability == TraitAbility.SWITCH_OPPONENT_CARD){
            TransferCard(battingListEleven[droppedIndex]);
        }

        if (battingListEleven[droppedIndex].TraitCard.Ability == TraitAbility.AFFECT_OPPONENT_HP){
            HalfOpponentHP();
        }

        if (battingListEleven[droppedIndex].TraitCard.Ability == TraitAbility.AFFECT_OPPONENT_ATTACK){
            HalfOpponentAttack();
        }
    }

    private void HalfOpponentAttack(){
        int tempIndex = Random.Range(0, bowlingListEleven.Count);
        bowlingListEleven[tempIndex].AttackPoint /= 2;
        UpdateBowlingCards();
    }

    private void HalfOpponentHP(){
        int tempIndex = Random.Range(0, bowlingListEleven.Count);
        bowlingListEleven[tempIndex].HitPoint /= 2;
        UpdateBowlingCards();
    }

    private void TransferCard(PlayerCard transferCard){
        PlayerCard tempCard = new PlayerCard();
        int tempIndex = Random.Range(0,bowlingListEleven.Count);

        tempCard = transferCard;
        transferCard = bowlingListEleven[tempIndex];
        bowlingListEleven[tempIndex] = tempCard;

        UpdateBowlingCards();
        battingSlot.GetChild(0).GetComponent<PlayerUI>().UpdateUI(transferCard);
    }

    public void SwitchOwnCard(PlayerCard batsman){

        Debug.Log("Prev index: " + this.batsman);

        if (batsman.HitPoint <= 6){
            int tempIndex = Random.Range(0, battingListEleven.Count);
            PlayerCard tempCard = new PlayerCard();

            tempCard = batsman; 
            batsman = battingListEleven[tempIndex];
            battingListEleven[tempIndex] = tempCard;


            //swap indexes
            int temp = tempIndex;
            tempIndex = this.batsman;
            this.batsman = temp;


            UpdateBattingCards();
            StartCoroutine(Wait(batsman));

            //this.batsman = tempIndex;
           

            Debug.Log("after index: " + this.batsman);

        }

       

       
    }
    IEnumerator Wait(PlayerCard batsman)
    {
        yield return new WaitForSeconds(1f);
        battingSlot.GetChild(0).GetComponent<PlayerUI>().UpdateUI(batsman);
        
    }

}
