﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class GameController : MonoBehaviour, IGameController{

    static GameController instance = null;
    bool secondInnings = false;
    bool batting;
    int target;
    int currentScore;
    Factory factory;
   // public BoardUIController board;

    public MatchSimUI matchSimUI;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            return;
        }
        Destroy(this.gameObject);
    }
	void Start () {

        GetComponent<Factory>().ReadFromCCGDatabase();
        TeamGenerator teamGenerator = new TeamGenerator();
        teamGenerator.CreateTeams();
        matchSimUI.Initialize();

	}
    public static GameController Controller(){
        return instance;
    }
	
	public bool GetSecondInnings() { return secondInnings; }
    public void SetIsSecondInnings(bool isSecondInnings) { secondInnings = isSecondInnings; }

    public void SetBatting(bool isBatting) { batting = isBatting; }
    public bool GetIsBatting() { return batting; }

    public void SetTarget(int theTarget) { target = theTarget; }
    public int GetTarget() { return target; }

    public void GetBestEleven()
    {
        throw new NotImplementedException();
    }

    public int CurrentScore
    {
        get { return currentScore; }
        set { currentScore = value; }
    }
}
