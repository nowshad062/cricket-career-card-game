﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class Slot : MonoBehaviour, IDropHandler {
    Text HP;
    Text AP;
    int cardIndex;
    //void Start()
    //{
    //    cardIndex = 0;
        
    //}

    public GameObject item {
        get {
            if (transform.childCount>0){
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!item)
        {
            DragHandler.Item.gameObject.transform.SetParent(transform);
            cardIndex = DragHandler.Item.gameObject.transform.GetSiblingIndex();
            Debug.Log("card index: " + cardIndex);
            
            DragHandler.Item.gameObject.SetActive(false);
        }
    }
    public int GetCardIndex(){
        return cardIndex;
    }
}
