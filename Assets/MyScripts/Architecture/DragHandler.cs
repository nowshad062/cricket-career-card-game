﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {


    public static GameObject Item;
    Vector3 startPosition;
    Transform startParent;
    public Transform TargetSlot;


    public void OnBeginDrag(PointerEventData eventData)
    {
        //Item = null;
        Item = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Item = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent != startParent)
        {
            transform.position = startPosition;
        }
        
    }

}
