﻿using UnityEngine;
using System.Collections;

public class ConstraintsScript : MonoBehaviour {
    public Transform battingHand;
    public Transform bowlingHand;
    public Transform battingSlot;
    public Transform bowlingSlot;
    public Transform battingBuffSlot;
    public Transform bowlingBuffSlot;
    public Transform buffHand;
    public Transform DeBuffHand;
    Transform ConstraintsPanel;
    Transform battingSelection;
    Transform bowlingSelection;
    Transform battingSlotOverlay;
    Transform bowlingSlotOverlay;
    Transform battingBuffOverlay;
    Transform bowlingBuffOverlay;
    Transform buffPanel;
    Transform deBuffPanel;


    void Awake(){
        ConstraintsPanel = GetComponent<Transform>();
        battingSelection = ConstraintsPanel.GetChild(0).GetComponent<Transform>();
        bowlingSelection = ConstraintsPanel.GetChild(1).GetComponent<Transform>();
        battingSlotOverlay = ConstraintsPanel.GetChild(2).GetComponent<Transform>();
        bowlingSlotOverlay = ConstraintsPanel.GetChild(3).GetComponent<Transform>();
        battingBuffOverlay = ConstraintsPanel.GetChild(4).GetComponent<Transform>();
        bowlingBuffOverlay = ConstraintsPanel.GetChild(5).GetComponent<Transform>();
        buffPanel = ConstraintsPanel.GetChild(6).GetComponent<Transform>();
        deBuffPanel = ConstraintsPanel.GetChild(7).GetComponent<Transform>();
    }
    public void Constraints(Transform a)
    {
        if (a == battingHand){
            UpdateConstraintsPanels(false, true, false, true, true, true, true, true);//
        }else if (a == bowlingHand)
        {
            UpdateConstraintsPanels(true, false, true, false, true, true, true, true);//
        }else if (a == battingSlot)
        {
            UpdateConstraintsPanels(true, true, false, true, true, true, true, true);//
        }else if (a == bowlingSlot)
        {
            UpdateConstraintsPanels(true, false, true, false, true, true, true, true);//
        }else if (a == battingBuffSlot)
        {
            UpdateConstraintsPanels(true, true, true, true, true, true, true, true);
        }else if (a == bowlingBuffSlot)
        {
            UpdateConstraintsPanels(true, true, true, true, true, true, true, true);
        }else if (a == buffHand)
        {
            UpdateConstraintsPanels(true, true, true, true, false, false, false, true);
        }else if (a == DeBuffHand)
        {
            UpdateConstraintsPanels(true, true, true, true, false, false, true, false);
        }
    }




    public void UpdateConstraintsPanels(bool a, bool b, bool c, bool d, bool e, bool f, bool g, bool h)
    {
        battingSelection.gameObject.SetActive(a);
        bowlingSelection.gameObject.SetActive(b);
        battingSlotOverlay.gameObject.SetActive(c);
        bowlingSlotOverlay.gameObject.SetActive(d);
        battingBuffOverlay.gameObject.SetActive(e);
        bowlingBuffOverlay.gameObject.SetActive(f); 
        buffPanel.gameObject.SetActive(g);
        deBuffPanel.gameObject.SetActive(h);

    }
}
