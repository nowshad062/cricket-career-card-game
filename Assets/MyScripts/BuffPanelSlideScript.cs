﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuffPanelSlideScript : MonoBehaviour {
    Button slideButton;
    public Sprite open;
    public Sprite close;
    public RuntimeAnimatorController openAnim;
    public RuntimeAnimatorController closeAnim;
    public RuntimeAnimatorController buttonOpenAnim;
    public RuntimeAnimatorController buttonCloseAnim;
    public Transform utilityPanel; 
    bool closed;

	void Start () {
        closed = true;
        slideButton = GetComponent<Button>();
        slideButton.onClick.AddListener(delegate { ButtonToggle(); });
	}
	
	void Open()
    {
        utilityPanel.GetComponent<Animator>().enabled = false;
        this.GetComponent<Animator>().enabled = false;
        utilityPanel.GetComponent<Animator>().runtimeAnimatorController = openAnim;
        this.GetComponent<Animator>().runtimeAnimatorController = buttonOpenAnim;
        utilityPanel.GetComponent<Animator>().enabled = true;
        this.GetComponent<Animator>().enabled = true;
        this.GetComponent<Image>().overrideSprite = close;
    }
    void Close()
    {
        utilityPanel.GetComponent<Animator>().enabled = false;
        this.GetComponent<Animator>().enabled = false;
        utilityPanel.GetComponent<Animator>().runtimeAnimatorController = closeAnim;
        this.GetComponent<Animator>().runtimeAnimatorController = buttonCloseAnim; 
        utilityPanel.GetComponent<Animator>().enabled = true;
        this.GetComponent<Animator>().enabled = true;
        this.GetComponent<Image>().overrideSprite = open;
    }

    void ButtonToggle()
    {
        if (closed)
        {
            Open();
            closed = false;
        }
        else {
            Close();
            closed = true;
        }
    }
}
