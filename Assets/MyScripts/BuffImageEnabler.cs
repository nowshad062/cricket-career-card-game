﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuffImageEnabler : MonoBehaviour {

    Transform self;
    public Transform buffImage;
	
    void Start()
    {
        self = GetComponent<Transform>();
    }
	// Update is called once per frame
	void Update () {
        if (self.childCount > 0 && self.GetChild(0).gameObject.activeInHierarchy == true)
        {
            buffImage.gameObject.SetActive(true);
        }else
            buffImage.gameObject.SetActive(false);


        
    }
    void BuffImageBlink()
    {
        Color c = buffImage.GetComponent<Image>().color;
        if (c.a == 0)
        {
            c.a = 250;
            buffImage.GetComponent<Image>().color = c;
        }
        else {
            c.a = 0;
            buffImage.GetComponent<Image>().color = c;
        }

    }
}
