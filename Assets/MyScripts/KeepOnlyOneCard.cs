﻿using UnityEngine;
using System.Collections;

public class KeepOnlyOneCard : MonoBehaviour {
    public Transform parentToReturn;
    public bool buffCardSlot;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.childCount > 1 ){
            if (transform.GetChild(1) != null && transform.GetChild(1).gameObject.activeInHierarchy==true){

                if (buffCardSlot)
                {
                    if (transform.GetChild(1).GetComponent<PlayerIndex>() != null && transform.GetChild(1).GetComponent<PlayerIndex>().GetBuff())
                    {
                        transform.GetChild(1).SetParent(parentToReturn.GetChild(0));
                    }
                    else
                        transform.GetChild(1).SetParent(parentToReturn.GetChild(1));
                }
                else
                {
                    transform.GetChild(1).SetParent(parentToReturn);

                }


            }

        }
	}
}
