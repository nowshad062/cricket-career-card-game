﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpriteManager : MonoBehaviour {
    public static SpriteManager spriteManager;

    public Sprite[] countryFlags;
    Dictionary<string, int> countryNameToIndex;

    public Sprite[] playerCategory;
    Dictionary<string, int> playerCategoryToIndex;


    void Awake(){
        if (spriteManager == null)
            spriteManager = this;

        countryNameToIndex = new Dictionary<string, int>();
        countryNameToIndex.Add(Country.AUSTRALIA.ToString(), 0);
        countryNameToIndex.Add(Country.BANGLADESH.ToString(), 1);
        countryNameToIndex.Add(Country.ENGLAND.ToString(), 2);
        countryNameToIndex.Add(Country.INDIA.ToString(), 3);
        countryNameToIndex.Add(Country.NEWZEALAND.ToString(), 4);
        countryNameToIndex.Add(Country.PAKISTAN.ToString(), 5);
        countryNameToIndex.Add(Country.SOUTHAFRICA.ToString(), 6);
        countryNameToIndex.Add(Country.SRILANKA.ToString(), 7);
        countryNameToIndex.Add(Country.WESTINDIES.ToString(), 8);
        countryNameToIndex.Add(Country.ZIMBABWE.ToString(), 9);

        playerCategoryToIndex = new Dictionary<string, int>();
        playerCategoryToIndex.Add(PlayerCategory.COMMON.ToString(), 0);
        playerCategoryToIndex.Add(PlayerCategory.RARE.ToString(), 1);
        playerCategoryToIndex.Add(PlayerCategory.LEGENDARY.ToString(), 2);
    }

    public Sprite GetCountryFlag(string countryName){
        return countryFlags[countryNameToIndex[countryName]];
    }

    public Sprite GetPlayerCategorySprite(string categoryName){
        return playerCategory[playerCategoryToIndex[categoryName]];
    }
}
