﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using CoreData;

public class BoardUIController : MonoBehaviour {

    public GameObject BattingPanel;
    public GameObject bowlingPanel;

    public List<GameObject> batsmans;
    public List<GameObject> bowlers;
    public GameObject playerBatting;
    public GameObject playerBowling;

    public GameObject buffpanel;
    public GameObject debuffpanel;
    public List<Button> buffButtons;
    public List<Button> debuffButtons;

    public Team battingTeam;
    public Team bowlingTeam;
    public Button playButton;

    PlayerManager playerManager;
    TeamManager teamManager;

    PlayerCard currentBatsman;
    PlayerCard currentBowler;
    int currentBatsmanIndex;
    int currentBowlerIndex;

    PlayerUI currentBatsmanUI;
    PlayerUI currentBowlerUI;
    Simulation simulation;
    RunScoringTable runScoringTable;
    int ballCounter = 0;

    private System.Random rng;


    int totalScore = 0;
    int over = 0;
    int runInThisBall = 0;

    //RunPanel
    public Text runInThisBallText;
    public Text overText;
    public Text scoreText;


    void Awake()
    {
        playerManager = Factory.getPlayerManager();
        teamManager = Factory.getTeamrManager();
        simulation = GetComponent<Simulation>();
        runScoringTable = new RunScoringTable();
        currentBatsmanUI =  playerBatting.GetComponent<PlayerUI>();
        currentBowlerUI = playerBowling.GetComponent<PlayerUI>();
        rng = new System.Random();

        playButton.onClick.AddListener(delegate
        {
            if (currentBatsman != null && currentBowler != null)
            {
                TypeMatchUpCalculation();

                GetComponent<Simulation>().Simulate(currentBatsman, currentBowler);

                currentBatsman.RestoreDefault();
                currentBowler.RestoreDefault();
                BallCalculation();
                RunCalculation(currentBatsman);
                UpdateUI();

                ListenDeadEvent();
  
            }

        });
    }

    public void Initialize()
    {
        battingTeam = Factory.getTeamrManager().getTeam(0);
        bowlingTeam = Factory.getTeamrManager().getTeam(2);

        FillUpBattingTeam(battingTeam);
        FillUpBowlingTeam(bowlingTeam);

        FillUpBuffCards(battingTeam);
        FillUpDebuffCards(battingTeam);
    }

    void FillUpBuffCards(Team battingTeam)
    {
        for (int i = 0; i < battingTeam.BuffCards.Count; i++)
        {

            Button buffButton = buffpanel.transform.GetChild(i).GetComponent<Button>();

            UtilityCard buffUtility = battingTeam.BuffCards[i];
            buffButton.GetComponent<UtilityCardUI>().UpdateUI(buffUtility);

            AddBuffListener(buffButton, i);
        }
    }

    void FillUpDebuffCards(Team battingTeam)
    {
        for (int i = 0; i < battingTeam.DebuffCards.Count; i++)
        {

            Button debuffButton = debuffpanel.transform.GetChild(i).GetComponent<Button>();

            UtilityCard debuffUtility = battingTeam.DebuffCards[i];
            debuffButton.GetComponent<UtilityCardUI>().UpdateUI(debuffUtility);

            AddDebuffListener(debuffButton, i);
        }
    }

    void FillUpBattingTeam(Team battingTeam)
    {
        for(int i=0; i<battingTeam.AllPlayers.Count; i++){

            PlayerCard player = playerManager.getPlayer(battingTeam.AllPlayers[i]);
            batsmans[i].GetComponent<PlayerUI>().UpdateUI(player);
            Button batsman = batsmans[i].GetComponent<Button>();
            AddBatsmanListener(batsman, i);

        }

    }

    void FillUpBowlingTeam(Team bowlingTeam)
    {
        for (int i = 0; i < bowlingTeam.AllPlayers.Count; i++)
        {
            PlayerCard player = playerManager.getPlayer(bowlingTeam.AllPlayers[i]);
            bowlers[i].GetComponent<PlayerUI>().UpdateUI(player);
            Button bowler = bowlers[i].GetComponent<Button>();
            AddBowlerListener(bowler, i);
        }
    }

    void AddBatsmanListener(Button b, int value)
    {
        b.onClick.AddListener(() => ChoseBatsman(value));
    }

    void AddBowlerListener(Button b, int value)
    {
        b.onClick.AddListener(() => ChoseBowler(value));
    }

    void AddBuffListener(Button b, int value)
    {
        b.onClick.AddListener(() => ActivateBuff(value));
    }

    void AddDebuffListener(Button b, int value)
    {
        b.onClick.AddListener(() => ActivateDebuff(value));
    }

    void ChoseBatsman(int i)
    {
        PlayerCard chosenPlayer = playerManager.getPlayer(battingTeam.AllPlayers[i]);

        if (currentBatsman == null){

            batsmans[i].SetActive(false);
            playerBatting.SetActive(true);

            if (chosenPlayer.isBowler())
            {
                HalfHP(chosenPlayer);
            }

            playerBatting.GetComponent<PlayerUI>().UpdateUI(chosenPlayer);
            currentBatsman = chosenPlayer;
            currentBatsmanIndex = i;
            
            simulation.SimulateOnce(currentBatsman, currentBowler);
            SelectRandomBowler();

        }
    }

    void ActivateBuff(int i)
    {
        UtilityCard buff = battingTeam.BuffCards[i];

        if (currentBatsman != null)
        {
            buff.Consume(currentBatsman);
            UpdateUI();
            ListenDeadEvent();
            /*currentBatsman.HitPoint += buff.health;
            currentBatsman.AttackPoint += buff.attack;
            playerBatting.GetComponent<PlayerUI>().UpdateUI(currentBatsman);
            ListenDeadEvent();*/
        }
    }

    void ActivateDebuff(int i)
    {
        UtilityCard debuff = battingTeam.DebuffCards[i];

        if (currentBowler != null)
        {
            debuff.Consume(currentBowler);
            UpdateUI();
            ListenDeadEvent();
            /*currentBowler.HitPoint -= debuff.health;
            currentBowler.AttackPoint -= debuff.attack;
            playerBowling.GetComponent<PlayerUI>().UpdateUI(currentBowler);
            ListenDeadEvent();*/
        }
    }

    void ChoseBowler(int i)
    {
        PlayerCard chosenPlayer = playerManager.getPlayer(bowlingTeam.AllPlayers[i]);

        if (currentBowler == null)
        {
            bowlers[i].SetActive(false);
            playerBowling.SetActive(true);
            if (!chosenPlayer.isBowler())
            {
                HalfHP(chosenPlayer);
            }
            playerBowling.GetComponent<PlayerUI>().UpdateUI(chosenPlayer);
            currentBowler = chosenPlayer;
            currentBowlerIndex = i;
        }
    }

    void SelectRandomBowler()
    {
        List<int> randomBowlerIndexes = new List<int>();

        for (int i = 0; i < bowlingTeam.AllPlayers.Count; i++)
        {
            PlayerCard randomBowler = playerManager.getPlayer(bowlingTeam.AllPlayers[i]);

            if (randomBowler.HitPoint > 0)
            {
                randomBowlerIndexes.Add(i);
            }
        }

        Shuffle(randomBowlerIndexes);
        int bIndex = randomBowlerIndexes[0];
        bowlers[bIndex].GetComponent<Button>().onClick.Invoke();
    }



    void SelectNewBowler()
    {
        SelectRandomBowler();
    }

    void UpdateUI()
    {
        currentBatsmanUI.UpdateUI(currentBatsman);
        currentBowlerUI.UpdateUI(currentBowler);
    }

    void BallCalculation()
    {
        this.ballCounter++;

        if ((ballCounter % 6) == 0)
        {
            over++;
            OverDone();
        }

        UpdateScoreBoard();
    }

    void OverDone()
    {
        //Over done
        RetireCurrentBowler();
        SelectNewBowler();
        Debug.Log("Over Done! Switch Bowler");
    }

    void RetireCurrentBowler()
    {
        playerBowling.SetActive(false);
        bowlers[currentBowlerIndex].SetActive(true);
        bowlers[currentBowlerIndex].GetComponent<PlayerUI>().UpdateUI(currentBowler);
        currentBowler = null;
    }

    void RunCalculation(PlayerCard batsman)
    {
        int random = Random.Range(0,100);
        runInThisBall = runScoringTable.getRunProbability(batsman.AttackPoint, random);
        totalScore = (totalScore + runInThisBall);

        UpdateScoreBoard();
  
    }

    void HalfHP(PlayerCard player)
    {
        player.HitPoint /= 2;
    }

    void TypeMatchUpCalculation()
    {

        PlayerType batsmanType = currentBatsman.PlayerType;
        PlayerType bowlerType = currentBowler.PlayerType;

        TypeMatch match = new TypeMatch();

        currentBatsman.AttackPoint += match.getBattingAttackIncrease(batsmanType, bowlerType);
        currentBowler.AttackPoint += match.getBowlingAttackIncrease(batsmanType, bowlerType);

    }

    void UpdateScoreBoard()
    {
        runInThisBallText.text = runInThisBall.ToString();
        overText.text = over.ToString()+ "." +  (this.ballCounter%6).ToString();
        scoreText.text = totalScore.ToString();
    }

    void ListenDeadEvent()
    {
        if (currentBatsman.HitPoint <= 0)
        {
            Debug.Log("Batsman Dead");
            currentBowler.TraitCard.IsDead = true;
            currentBatsman = null;
            playerBatting.SetActive(false);

        }

        if (currentBowler.HitPoint <= 0)
        {
            Debug.Log("bowler Dead");
            currentBowler.TraitCard.IsDead = true;
            currentBowler = null;
            playerBowling.SetActive(false);
            SelectNewBowler();
        }
    }

    public void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
